package com.app.Loaders;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.app.Objects.Categories;
import com.app.Utils.JSONParser;
import com.app.Utils.Utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import android.widget.Toast;

public class CategoryLoader extends AsyncTaskLoader<List<Categories>> {

    Activity _activity = null;
    Context _context = null;
    String deviceId = null;

    public CategoryLoader(Context context) {
        super(context);
        this._context = context;
        this._activity = (Activity) context;
        deviceId = Utils.getDeviceImei(context);
    }

    @Override
    public List<Categories> loadInBackground() {

        List<Categories> categoryList = new ArrayList<Categories>();
        String url = "http://panel.iptv.rocks/service/category_list.php?macaddress=" + deviceId;
        Log.e("category_url", url);
        // String url =
        // "http://panel.iptv.rocks/service/json.php?macaddress='"+deviceId+"'";

        try {

            // Creating JSON Parser instance
            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url);
            JSONArray jsonArray = json.getJSONArray("category_list");

            Categories category = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                category = new Categories();

                JSONObject result = jsonArray.getJSONObject(i);

                int categoryId = result.getInt("category_id");
                String categoryName = result.getString("category_name");
                String imageUrl = result.getString("category_logo");

                category.setCategoryId(categoryId);
                category.setCategoryName(categoryName);
                category.setCategoryUrl(imageUrl);

                if (result.has("sub_category_list")) {
                    ArrayList<Categories> subList = new ArrayList<Categories>();
                    Categories subCatrories = null;
                    JSONArray subArray = result.getJSONArray("sub_category_list");
                    for (int j = 0; j < subArray.length(); j++) {
                        subCatrories = new Categories();
                        JSONObject subResult = subArray.getJSONObject(j);
                        int subCatId = subResult.getInt("category_id");
                        String subName = subResult.getString("category_name");
                        String subUrl = subResult.getString("category_logo");

                        subCatrories.setCategoryId(subCatId);
                        subCatrories.setCategoryName(subName);
                        subCatrories.setCategoryUrl(subUrl);
                        subList.add(subCatrories);
                    }
                    category.setSubCategories(subList);
                }

                categoryList.add(category);
            }

        } catch (Exception e) {

        }

        for (Categories name : categoryList) {
            // Toast.makeText(this, "Your device id is : " + Utils.getDeviceId(), Toast.LENGTH_LONG).show();
            Log.e("Category name: ", name.getCategoryName());
        }

        return categoryList;
    }

}
