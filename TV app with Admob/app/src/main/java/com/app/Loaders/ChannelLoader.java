package com.app.Loaders;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.app.Fragments.ChannelFragment;
import com.app.Objects.Channel;
import com.app.Utils.JSONParser;
import com.app.Utils.Utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;


public class ChannelLoader extends AsyncTaskLoader<List<Channel>>{

	Activity _activity = null;
	Context _context = null;
	String deviceId = null;
	
	public ChannelLoader(Context context) {
		super(context);
		this._context = context;
		this._activity = (Activity) context;
		deviceId = Utils.getDeviceImei(context);
	}

	@Override
	public List<Channel> loadInBackground() {
		List<Channel> channelList = new ArrayList<Channel>();
		Log.e("category id", ChannelFragment._categoryId);
		String id = deviceId;
		String categoryId = "&category_id="+ChannelFragment._categoryId+"";
		String url = "http://panel.iptv.rocks/service/channel_list.php?macaddress="+id+categoryId+"&subcategory_id="+ChannelFragment._subcategoryId;
		Log.e("channel url", url);
		//String url = "http://panel.iptv.rocks/service/json.php?macaddress='"+deviceId+"'";

try {
			
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);
			JSONArray jsonArray = json.getJSONArray("channel_list");

			Channel channel = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				channel = new Channel();

				JSONObject result = jsonArray.getJSONObject(i);

				String title = result.getString("channel_name");
				String streamurl = result.getString("channel_stream");
				String description = result.getString("channel_description");
				String imageUrl = result.getString("channel_logo");
				
				channel.setTitle(title);
				channel.setDescription(description);
				channel.setStreamUrl(streamurl);
				channel.setImage(imageUrl);
				channelList.add(channel);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

          for(Channel name : channelList){
        	  Log.e("Channel Name", name.getTitle());
          }


		
		return channelList;
	}
	

}
