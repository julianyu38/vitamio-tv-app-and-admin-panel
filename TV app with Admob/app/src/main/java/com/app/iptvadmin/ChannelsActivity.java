package com.app.iptvadmin;

import com.app.Fragments.ChannelFragment;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class ChannelsActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        setContentView(com.app.iptvadmin.R.layout.channel_activity_layout);
        int id = getIntent().getIntExtra("id", 0);
        Bundle arguments = new Bundle();
        arguments.putString(ChannelFragment.CATEGORY_ID, String.valueOf(id));

        ChannelFragment fragment = new ChannelFragment(this);
        fragment.setArguments(arguments);

        getSupportFragmentManager().beginTransaction()
                .replace(com.app.iptvadmin.R.id.content_frame, fragment).commit();
    }

}
