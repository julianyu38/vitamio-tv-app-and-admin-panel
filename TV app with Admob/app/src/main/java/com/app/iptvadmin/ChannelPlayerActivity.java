package com.app.iptvadmin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class ChannelPlayerActivity extends Activity implements
        OnCompletionListener, OnPreparedListener {

    VideoView videoview;
    MediaController mc;
    String url = "";
    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // disable screen saver


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.channel_player_activity);
        io.vov.vitamio.Vitamio.isInitialized(getApplicationContext());
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            url = extras.getString("stream_url");
            Log.e("Stream Url", url);
        }
        playVideo(url);
        mInterstitialAd = new InterstitialAd(ChannelPlayerActivity.this);

        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        // AdRequest adRequest = new AdRequest.Builder()
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("AFC6CA7221788F8DDAD93C8D0BD88F56")
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    private void playVideo(String streamLink) {
        try {

            videoview = (VideoView) findViewById(com.app.iptvadmin.R.id.video_player);
            mc = new MediaController(this);

            videoview.setVideoURI(Uri.parse(streamLink));
            videoview.setOnCompletionListener(this);
            videoview.setOnPreparedListener(this);
            videoview.setMediaController(mc);
        } catch (Exception e) {

            System.out.println("Video Play Error :" + e.toString());
            finish();

        }
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e("on Error", "IO Error e=" + what + " x=" + extra);

        new AlertDialog.Builder(getBaseContext())
                .setTitle("Error")
                .setMessage(
                        "Please contact your IPTV Administrator to Activate your TV BOX")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();

        return true;
    }

    @Override
    public void onPrepared(MediaPlayer arg0) {
        startVideoPlayback();
        mc.show();
        Log.e("prepared", "prepared");
    }

    private void startVideoPlayback() {
        if (videoview != null) {
            videoview.start();
        }

    }

    @Override
    public void onCompletion(MediaPlayer arg0) {
        Log.e("Complete", "Complete");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /*@Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }*/

    @Override
    public void onDestroy() {
       /* if (mAdView != null) {
            mAdView.destroy();
        }*/
        super.onDestroy();
    }

}
