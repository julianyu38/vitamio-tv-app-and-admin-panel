package com.app.iptvadmin;

import com.app.Utils.Utils;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

public class DeviceIdPopup extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(com.app.iptvadmin.R.layout.id_popup);
        TextView tv_id = (TextView) findViewById(com.app.iptvadmin.R.id.tvDeviceId);
        tv_id.setText(Utils.getDeviceImei(this));
    }
}
