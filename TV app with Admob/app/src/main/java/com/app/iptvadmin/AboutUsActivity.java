package com.app.iptvadmin;

import com.app.Fragments.ContentFragment;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class AboutUsActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(com.app.iptvadmin.R.layout.about_us_layout);

        ContentFragment fragment = new ContentFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(com.app.iptvadmin.R.id.about_us_frame, fragment).commit();
    }
}
