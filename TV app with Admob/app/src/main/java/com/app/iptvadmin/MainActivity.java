package com.app.iptvadmin;

import com.app.Fragments.CategoryListFragment;
import com.app.Fragments.ChannelFragment;
import com.app.Utils.ConnectivityTester;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

//test id: AFC6CA7221788F8DDAD93C8D0BD88F56
//import com.appnext.ads.interstitial.Interstitial;
/*
* Placement ID b79b68bf-e295-47c0-9425-1015725e69b3
*
* */
/*home 360*250*/
/*
* App ID: ca-app-pub-5636432951018969~5863721931
*Ad unit name: home 360*250
*Ad unit ID: ca-app-pub-5636432951018969/8817188336
*
*
* Ad unit name: home fullscreen
*Ad unit ID: ca-app-pub-5636432951018969/2770654732
*
* */
public class MainActivity extends FragmentActivity implements
        CategoryListFragment.Callbacks {

    ConnectivityTester internet;
    private AdView mAdView;

    //TODO -UNCOMMENT THIS LINE TO SHOW INTERSTITIAL AD -_-
    //InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.app.iptvadmin.R.layout.activity_main);
        TextView txt_deviceId = (TextView) findViewById(com.app.iptvadmin.R.id.tex_device_id);
        txt_deviceId.setText("");

        CategoryListFragment listFragment = new CategoryListFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(com.app.iptvadmin.R.id.category_list, listFragment).commit();
        internet = new ConnectivityTester(this);
        if (internet.isConnected()) {
        }
        mAdView = (AdView) findViewById(R.id.banner_AdView);
        // THIS LINE IS FOR REAL AD ID
        //AdRequest adRequest = new AdRequest.Builder().build();

        //THIS LINES IS FOR TEST IDs
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("AFC6CA7221788F8DDAD93C8D0BD88F56")
                .build();
        mAdView.loadAd(adRequest);
        //TODO -UNCOMMENT THIS PORTION TO SHOW INTERSTITIAL AD -_-
        /*mInterstitialAd = new InterstitialAd(ChannelPlayerActivity.this);

        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        AdRequest adRequest = new AdRequest.Builder().build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });*/
    }
    //TODO -UNCOMMENT THIS METHOD FOR INTERSTITIAL AD USE -_-
    /*private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }*/

    int flag = 0;

    @Override
    public void onCategorySelected(int id, int subId) {
        Log.e("sub_id", "" + subId);
        ChannelFragment._subcategoryId = "" + subId;
        if (flag != 0) {
            FrameLayout frame = (FrameLayout) findViewById(com.app.iptvadmin.R.id.content_frame);
            if (frame != null) {
                Bundle arguments = new Bundle();
                arguments.putString(ChannelFragment.CATEGORY_ID,
                        String.valueOf(id));

                ChannelFragment fragment = new ChannelFragment(this);
                fragment.setArguments(arguments);

                getSupportFragmentManager().beginTransaction()
                        .replace(com.app.iptvadmin.R.id.content_frame, fragment).commit();
            } else {
                Intent i = new Intent(this, ChannelsActivity.class);
                i.putExtra("id", id);
                startActivity(i);
            }
        }
        flag = 1;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.app.iptvadmin.R.menu.main, menu);
        menu.getItem(0).setTitle("Get Device Id");
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {

        switch (item.getItemId()) {
            case com.app.iptvadmin.R.id.about_us:
                Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
                startActivity(intent);
                break;
            case com.app.iptvadmin.R.id.action_search:
                Intent i = new Intent(this, DeviceIdPopup.class);
                startActivity(i);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}
