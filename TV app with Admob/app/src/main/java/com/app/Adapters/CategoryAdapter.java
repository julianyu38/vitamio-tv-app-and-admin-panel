package com.app.Adapters;

import java.util.ArrayList;
import java.util.List;
import com.app.Objects.Categories;
import com.app.Utils.ImageLoader;
import com.app.iptvadmin.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoryAdapter extends ArrayAdapter<Categories>{
	
	private Context CurrentContext;
	private Categories CurrentItem;
	private ArrayList<Categories> items;
	private final ImageLoader imageLoader;

	public CategoryAdapter(Context context, int textViewResourceId,
			List<Categories> objects) {
		super(context, textViewResourceId, objects);
		
		this.imageLoader = new ImageLoader(context,
				R.drawable.icon_video, R.drawable.icon_video);
		
		this.items = (ArrayList<Categories>) objects;
		this.CurrentContext = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if ((this.items == null) || (position + 1 > this.items.size()))
			return convertView;

		this.CurrentItem = ((Categories) this.items.get(position));
		LayoutInflater vi = (LayoutInflater) CurrentContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = vi.inflate(R.layout.category_list_item, null);
		
		TextView categoryName = (TextView) convertView
				.findViewById(R.id.category_name_txt);
		ImageView categoryImage = (ImageView)convertView
				.findViewById(R.id.img_list_category);
		
		categoryName.setText(CurrentItem.getCategoryName());
		if(CurrentItem.getCategoryUrl() != null){
			categoryImage.setTag(imageLoader);
			imageLoader.displayImage(CurrentItem.getCategoryUrl(), categoryImage);
		}else{
			categoryImage.setImageResource(R.drawable.icon_video);
		}
			
		return convertView;
	}
}
