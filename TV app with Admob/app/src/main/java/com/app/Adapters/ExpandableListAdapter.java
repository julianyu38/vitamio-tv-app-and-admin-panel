package com.app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Objects.Categories;
import com.app.Utils.ImageLoader;
import com.app.iptvadmin.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Muhmmad Waqas on 3/9/2016.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter{
    private Context _context;
    private List<Categories> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<Categories, List<Categories>> _listDataChild;

    private final ImageLoader imageLoader;

    public ExpandableListAdapter(Context context, List<Categories> listDataHeader,
                               HashMap<Categories, List<Categories>> listChildData) {

        this.imageLoader = new ImageLoader(context,
                R.drawable.icon_video, R.drawable.icon_video);

        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = infalInflater.inflate(R.layout.categories_child_list_item, parent, false);
        final Categories childItem = (Categories) getChild(groupPosition, childPosition);
        TextView categoryName = (TextView) rowView
                .findViewById(R.id.child_category_name_txt);
        ImageView categoryImage = (ImageView)rowView
                .findViewById(R.id.child_img_list_category);

        categoryName.setText(childItem.getCategoryName());
        if(childItem.getCategoryUrl() != null){
            categoryImage.setTag(imageLoader);
            imageLoader.displayImage(childItem.getCategoryUrl(), categoryImage);
        }else{
            categoryImage.setImageResource(R.drawable.icon_video);
        }
        return rowView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Categories parentItem = (Categories) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(
                    R.layout.category_list_item, parent, false);
        }

        TextView categoryName = (TextView) convertView
                .findViewById(R.id.category_name_txt);
        ImageView categoryImage = (ImageView)convertView
                .findViewById(R.id.img_list_category);

        categoryName.setText(parentItem.getCategoryName());
        if(parentItem.getCategoryUrl() != null){
            categoryImage.setTag(imageLoader);
            imageLoader.displayImage(parentItem.getCategoryUrl(), categoryImage);
        }else{
            categoryImage.setImageResource(R.drawable.icon_video);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
