package com.app.Fragments;

import com.app.Loaders.ContentLoader;
import com.app.Utils.ConnectivityTester;
import com.app.iptvadmin.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ContentFragment extends Fragment implements
		LoaderManager.LoaderCallbacks<String> {
	private TextView content_txt;
	private static View _rootView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ConnectivityTester internet = new ConnectivityTester(getActivity());
		if(internet.isConnected()) {
		getActivity().getSupportLoaderManager().initLoader(0, null, this);
		getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
		}
		else {
			internet.detectInternetConnection();
		}
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		_rootView = inflater.inflate(R.layout.about_us_fragment, container, false);
		return _rootView;
	}

	@Override
	public Loader<String> onCreateLoader(int arg0, Bundle arg1) {
		return new ContentLoader(getActivity());
	}

	@Override
	public void onLoadFinished(Loader<String> arg0, String content) {
		if(content != null){
			this.content_txt = ((TextView)_rootView.findViewById(R.id.txt_content));
			content_txt.setText(content);
		}
	}

	@Override
	public void onLoaderReset(Loader<String> arg0) {

	}

}
