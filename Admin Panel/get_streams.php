<?php 
include 'head.php';
if(isset($_SESSION['user_info'])){
?>
            <!-- PAGE CONTENT -->
            <div class="page-content">
               <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search...">
                        </form>
                    </li>   
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="<?php echo SITE_URL; ?>logout.php" class="mb-control" data-box="#mb-signout" style="width:100px"><span class="fa fa-sign-out"></span> Logout</a>
                    </li> 
                    <!-- END SIGN OUT -->         
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>                    
                    <li><a href="#">Streams</a></li>
                    <li class="active">Channels Streams</li>
                </ul>
                <!-- END BREADCRUMB -->

                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span>Channels Streams</h2>
                </div>
                <!-- END PAGE TITLE -->                

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
					 <!-- START RESPONSIVE TABLES -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h3 class="panel-title">Get Channels Streams</h3>
                                </div>

                                <div class="panel-body panel-body-table">

                                    <div class="table-responsive">
									<!--<table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable_1">
<iframe src="http://database.freetuxtv.net/" name="channels" scrolling="auto" frameborder="no" align="center" height = "1200px" width = "100%">
</iframe>
</tbody>
</table>-->
                                        <!--<textarea rows="50" cols="185" style="border:none;">
											<?php
												$curl = curl_init();
												curl_setopt_array($curl, array(
													CURLOPT_RETURNTRANSFER => 1,
													CURLOPT_URL => 'http://database.freetuxtv.net/playlists/list?format=xml'
												));
												echo $result = curl_exec($curl);
												
											?>
                                            
                                        </textarea>-->
									<div class="panel-body panel-body-table">
                                    
                                    <div class="table-responsive">
									<table class="table table-bordered table-striped">
										<thead>
										<tr>
											<td>Name</td>
											<td>Links</td>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td>Web TV (No lang)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_none.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (No lang)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_none.m3u</td>
										</tr>
										<tr>
											<td>Web Programmes (No lang)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_programmes_none.m3u</td>
										</tr>
										<tr>
											<td>Web Cam (No lang)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webcam_none.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Afrikaans)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_af.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Albanian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_sq.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Amharic)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_am.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Arabic)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ar.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Arabic)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_ar.m3u</td>
										</tr>
										<tr>
											<td>Web Cam (Arabic)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webcam_ar.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Armenian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_hy.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Azerbaijani)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_az.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Bengali/Bangla)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_bn.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Bengali/Bangla)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_bn.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Bulgarian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_bg.m3u</td>
										</tr>
										<tr>
											<td>Web Cam (Bulgarian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webcam_bg.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Cambodian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_km.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Catalan)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ca.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Catalan)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_ca.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Chinese)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_zh.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Chinese)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_zh.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Croatian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_hr.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Croatian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_hr.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Czech)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_cs.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Czech)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_cs.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Danish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_da.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Dutch)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_nl.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Dutch)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_nl.m3u</td>
										</tr>
										<tr>
											<td>Web Cam (Dutch)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webcam_nl.m3u</td>
										</tr>
										<tr>
											<td>Web TV (English)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_en.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (English)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_en.m3u</td>
										</tr>
										<tr>
											<td>Web Programmes (English)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_programmes_en.m3u</td>
										</tr>
										<tr>
											<td>Web Cam (English)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webcam_en.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Estonian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_et.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Finnish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_fi.m3u</td>
										</tr>
										<tr>
											<td>Web TV (French)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_fr.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (French)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_fr.m3u</td>
										</tr>
										<tr>
											<td>Web Programmes (French)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_programmes_fr.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Georgian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ka.m3u</td>
										</tr>
										<tr>
											<td>Web TV (German)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_de.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (German)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_de.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Greek)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_el.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Greek)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_el.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Hebrew)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_iw.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Hebrew)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_iw.m3u</td>
										</tr>
										<tr>
											<td>Web Cam (Hebrew)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webcam_iw.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Hindi)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_hi.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Hindi)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_hi.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Hungarian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_hu.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Indonesian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_in.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Interlingua)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_ia.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Irish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ga.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Irish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_ga.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Italian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_it.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Italian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_it.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Kazakh)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_kk.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Korean)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ko.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Kurdish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ku.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Latvian/Lettish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_lv.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Lithuanian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_lt.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Lithuanian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_lt.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Macedonian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_mk.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Macedonian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_mk.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Malay)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ms.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Maltese)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_mt.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Maltese)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_mt.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Moldavian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_mo.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Mongolian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_mn.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Norwegian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_no.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Persian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_fa.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Polish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_pl.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Polish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_pl.m3u</td>
										</tr>
										<tr>
											<td>Web Cam (Polish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webcam_pl.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Portuguese)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_pt.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Portuguese)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_pt.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Romanian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ro.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Romanian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_ro.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Russian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ru.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Russian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_ru.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Serbian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_sr.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Serbian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_sr.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Serbo-Croatian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_sh.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Serbo-Croatian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_sh.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Slovak)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_sk.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Slovak)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_sk.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Slovenian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_sl.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Slovenian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_sl.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Somali)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_tr.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Turkmen)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_tk.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Spanish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_es.m3u</td>
										</tr>
										<tr>
											<td>Web Programmes (Spanish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_programmes_es.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Swedish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_sv.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Tagalog)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_tl.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Thai)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_th.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Turkish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_tr.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Turkish)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_tr.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Turkmen)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_tk.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Ukrainian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_uk.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Ukrainian)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_uk.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Urdu)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_ur.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Vietnamese)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webtv_vi.m3u</td>
										</tr>
										<tr>
											<td>Web TV (Vietnamese)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_vi.m3u</td>
										</tr>
										<tr>
											<td>Web Radio (Vietnamese)</td>
											<td>http://database.freetuxtv.net/playlists/playlist_webradio_none.m3u</td>
										</tr>					
										</tbody>
									</table>
								</div>
								</div>
								</div>                                

                                </div>
                            </div>                                                

                        </div>
                    </div>
                    <!-- END RESPONSIVE TABLES -->
                </div>
                <!-- PAGE CONTENT WRAPPER -->                               
                                               
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="logout.php" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
      <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->
        
        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/demo_tables.js"></script>     
        <!-- END THIS PAGE PLUGINS-->  
        
        <!-- START TEMPLATE -->

        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->   
	<script>
	$(document).ready(function(){
		$("ul.streams li:first-child").addClass("active");
		$("#streams_li").addClass("active");
	});
	</script>
    </body>
</html>
<?php }else{
	
	echo "You are not authorized to visit this page direclty,Sorry";
} ?>




