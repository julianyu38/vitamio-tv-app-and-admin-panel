<?php 
include 'head.php';
if(isset($_SESSION['user_info'])){
	//print_r($_SESSION['user_info']['user_full_name']);exit;
?>
<!-- PAGE CONTENT -->

<div class="page-content">
<?php
$con = new db_connect();
$auth = new Select_DB();
$connection=$con->connect();
if($connection==1){
 $get = new Select_DB();
 $and = ($_SESSION['user_info']['user_type'] != 1) ? " AND user_id_fk = " . $_SESSION['user_info']['user_id'] : " AND user_id != 1 ";
 
 $result=$get->select_channel("tbl_user WHERE user_type = 2 $and");
 //$count=count($rows);
 //print_r($rows);
}
if( isset($_GET['action']) && $_GET['action'] == 'inactive' && !empty($_GET['action']) ){
	$auth->inactiveReseller($_GET['id']);
	unset($_GET['action']);
	echo "<script>location.href='reseller_management.php'</script>";
}
if( isset($_GET['action']) && $_GET['action'] == 'active' && !empty($_GET['action']) ){
	$auth->activeReseller($_GET['id']);
	unset($_GET['action']);
	echo "<script>location.href='reseller_management.php'</script>";
}
if( isset($_GET['action']) && $_GET['action'] == 'delete' && !empty($_GET['action']) ){
	mysql_query("DELETE FROM `tbl_user` WHERE `user_id` = " . $_GET['id']) or die( mysql_error() );
	unset($_GET['action']);
	echo "<script>location.href='reseller_management.php'</script>";
}
  ?>
  <!-- START X-NAVIGATION VERTICAL -->
  <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
    <!-- TOGGLE NAVIGATION -->
    <li class="xn-icon-button"> <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a> </li>
    <!-- END TOGGLE NAVIGATION --> 
    <!-- SIGN OUT -->
    <li class="xn-icon-button pull-right"> <a href="<?php echo SITE_URL; ?>logout.php" class="mb-control" data-box="#mb-signout" style="width:100px"><span class="fa fa-sign-out"></span> Logout</a> </li>
    <!-- END SIGN OUT -->
    
  </ul>
  <!-- END X-NAVIGATION VERTICAL --> 
  
  <!-- START BREADCRUMB -->
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_URL; ?>admin.php">Dashboard</a></li>
    <li><a href="#">Reseller Management</a></li>
    <li class="active">Reseller Client</li>
  </ul>
  <!-- END BREADCRUMB --> 
  
  <!-- PAGE TITLE -->
  <div class="page-title">
    <h2><span class="fa fa-arrow-circle-o-left"></span>Reseller Management</h2>
  </div>
  <!-- END PAGE TITLE --> 
  
  <!-- PAGE CONTENT WRAPPER -->
  <div class="page-content-wrap">
    <div class="row">
      <div class="col-md-12"> 
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Reseller Management Here</h3>
            <ul class="panel-controls">
              <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
              <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
              <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
            </ul>
          </div>
          <div class="panel-body">
            <table class="table datatable">
              <thead>
                <tr>
                  <th>Sr. #</th>
                  <th>Client Name</th>
                  <th>Client Username</th>
                  <th>User Limit</th>
                  <th>Client Password</th>
                  <th>Client Type</th>
                  <th>Add Date / Time</th>
                  <th>Status</th>
                  <th>Actions</th> 
                </tr>
              </thead>
              <tbody>
                <?php 
				$no =1;
				while($row=mysql_fetch_array($result)){ 
				?>
                <tr>
                  <td ><?php echo $no; ?></td>
                  <td style="cursor:pointer;"><?php echo $row['user_full_name']; ?></td>
                  <td style="cursor:pointer;"><?php echo $row['user_name']; ?></td>
                  <td><?php echo $row['reseller_user_limit']; ?></td>
                  <td style="cursor:pointer;"><?php echo $row['password']; ?></td>
                  <?php
				  $userType = "";
				  if($row['user_type'] == 1){
					  $userType = "Admin";
				  }
				  else if($row['user_type'] == 2) {
					  $userType = "Reseller";
				  }
				  else{
					  $userType = "User";
				  }
				  ?>
                  <td style="cursor:pointer;"><?php echo $userType; ?></td>
                  <td style="cursor:pointer;"><?php echo $row['user_add_date']; ?></td>
                  
                  
                  <?php 
				  if($row['status'] ==1){
					  $status = '<a href="reseller_management.php?action=inactive&id='.$row['user_id'].'" class="btn btn-danger btn-rounded btn-sm">Inactive</a>';
				  }
				  else {
					  $status = '<a href="reseller_management.php?action=active&id='.$row['user_id'].'" class="btn btn-default btn-rounded btn-sm">Active</a>';
				  }?>
                  <td style="cursor:pointer;"><?php echo $status; ?></td>
                  
                  <td><a href="edit_reseller.php?id=<?php echo $row['user_id'];  ?>" class="btn btn-default btn-rounded btn-sm">
                  <span class="fa fa-pencil"></span></a>
                  
                    <a href="reseller_management.php?action=delete&id=<?php echo $row['user_id'];  ?>" onclick="return confirm('Are you sure want to delete');" class="btn btn-danger btn-rounded btn-sm"><span class="fa fa-times"></span>
                    </a></td> 
                </tr>
                <?php 
					$no++;
				} ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END DEFAULT DATATABLE --> 
      </div>
    </div>
  </div>
  <!-- PAGE CONTENT WRAPPER --> 
  
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER --> 

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
  <div class="mb-container">
    <div class="mb-middle">
      <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
      <div class="mb-content">
        <p>Are you sure you want to log out?</p>
        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
      </div>
      <div class="mb-footer">
        <div class="pull-right"> <a href="logout.php" class="btn btn-success btn-lg">Yes</a>
          <button class="btn btn-default btn-lg mb-control-close">No</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MESSAGE BOX--> 

<!-- START PRELOADS -->
<audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS --> 

<!-- START SCRIPTS --> 
<!-- START PLUGINS --> 
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script> 
<!-- END PLUGINS --> 

<!-- START THIS PAGE PLUGINS--> 
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script> 
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script> 
<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script> 
<!-- END PAGE PLUGINS --> 

<!-- START TEMPLATE --> 
<script type="text/javascript" src="js/plugins.js"></script> 
<script type="text/javascript" src="js/actions.js"></script> 
<!-- END SCRIPTS --> 
<script>
$(document).ready(function(){
$("ul.reseller li:first-child").addClass("active");
$("#reseller_li").addClass("active");
});
</script>
</body></html><?php }else{
	echo "You are not authorized to visit this page direclty,Sorry";
} ?>
