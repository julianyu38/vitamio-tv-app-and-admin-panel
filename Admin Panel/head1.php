<?php
session_start();
include("common/functions.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		
        <title>IPTV-Admin Panel Login</title>
<?php
if(isset($_SESSION['user_info'])){
//date_default_timezone_set("Europe/Skopje"); 
//include("common/db_con.php");
//include("common/connection_class.php");

function curPageName() {
return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
}
function yubi_hex2bin($h)
  {
  if (!is_string($h)) return null;
  $r='';
  for ($a=0; $a<strlen($h); $a+=2) { $r.=chr(hexdec($h{$a}.$h{($a+1)})); }
  return $r;
  }
?>

		
		<!-- ==================================================================================== 
			STYLES BEGIN 
		===================================================================================== -->
		
		<!-- Global styles -->
		<link rel="stylesheet" type="text/css" href="css/reset.css" />
		<link rel="stylesheet" type="text/css" href="css/grid.css" />
		<link rel="stylesheet" type="text/css" href="css/config.css" />

		<!-- Plugin configuration (styles) -->
		<link rel="stylesheet" href="css/plugin_config.css" />
		
		<!--[if IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
		
		
		<!-- ======================================================================================
			SCRIPTS BEGIN
		======================================================================================= -->
        
	<!-- = Global Scripts [required for template] 
		***************************************************************************************-->
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/global_plugins_scripts.js"></script>
		
		
		
	<!-- = From Plugins Dir 
		***************************************************************************************-->
		
		<script type="text/javascript" src="plugins/lightbox/js/lightbox/jquery.lightbox.min.js"></script>
		<script type="text/javascript" src="plugins/jqueryui/all/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="plugins/validator/js/languages/jquery.validationEngine-en.js"></script>
		<script type="text/javascript" src="plugins/validator/js/jquery.validationEngine.js"></script>
		<script type="text/javascript" src="plugins/dialogs/jquery-fallr-1.2.js"></script>
		<script type="text/javascript" src="plugins/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
		<script type="text/javascript" src="plugins/spin/jquery-spin.js"></script>
		<script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="plugins/plupload/js/browserplus-min.js"></script>
		<script type="text/javascript" src="plugins/plupload/js/plupload.full.js"></script>
		<script type="text/javascript" src="plugins/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
		<script type="text/javascript" src="plugins/multiselect/js/ui.multiselect.js"></script>			
		<script type="text/javascript" src="plugins/datatables/media/js/jquery.dataTables.js"></script>	
		<script type="text/javascript" src="plugins/alerts/javascript/jquery.toastmessage.js"></script>	
		<script type="text/javascript" src="plugins/prettify/prettify.js"></script>
		<script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
				
		
		

	
		
	<!-- = From JS Dir
		****************************************************************************************-->
		
		<script type="text/javascript" src="js/modernizr.custom.js"></script>
		<script type="text/javascript" src="js/jquery.autogrowtextarea.js"></script>
		<script type="text/javascript" src="js/jquery.autotab-1.1b.js"></script>
		
		<!-- From JS Dir [plugin initialization] -->
		<script type="text/javascript" src="js/dialog_fallr_init.js"></script>
		<script type="text/javascript" src="js/tiny_mce_init.js"></script>
        <script type="text/javascript" src="js/datatables_init2.js"></script>
 
		<script type="text/javascript" src="js/datatables_init.js"></script>
		<script type="text/javascript" src="js/head_scripts.js"></script>
      
    </head>
    <body>
	<?php $logout = new Select_DB();
	?>	

<section id="layout">
<div class="logo_profile container_12">
<div class="grid_6 logo_img">
<img alt="Logo" height="74" src="images/logo.png">
</div>
<div class="grid_6 profile_meta">
<div class="user_meta">
<div>
<img alt="" src="images/admin.png">
</div>
<div class="name">
Welcome <?php echo $_SESSION['user_info']['user_name']; ?> <br>
<a class="submeta" href="#">Profile</a>
<a class="submeta" href="logout.php">Logout</a>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<section id="top" style="top:0px;">					
<section id="top_bar">						
<section id="main_menu">
<ul class="sf-menu">
<li><a href="admin.php">Dashboard</a></li>
<li style="cursor:pointer;"><a href="admin.php">Channel Management</a>
<ul>
<li><a href="add_chennel.php">Add Channel</a></li>
</ul>
</li>
<li style="cursor:pointer;"><a href="category_management.php">Category Management</a>
<ul>
<li><a href="add_category.php">Add Category</a></li>
</ul>
</li>
<li style="cursor:pointer;"><a href="device_management.php">Device Management</a>
<ul>
<li><a href="add_device.php">Add Device</a></li>
</ul>
</li>
<li><a href="attach_device.php">Device Activation</a></li>
<li style="cursor:pointer;"><a href="content_management.php">Content Management</a>
</li>
</ul>						
					<div class="clear"></div>
					</section><!-- End of #main_menu -->
				</section><!-- End of #top_bar -->
				<div class="clear"></div>
				
				<section class="top_in">	
					<section id="second_top_bar">
<div id="quick_task" class="jThumbnailScroller">
<div style="width: 1599px;" class="jTscrollerContainer">
<div class="clear"></div>
<ul style="top: 0px;" class="jTscroller">
<li><a href="admin.php"><span class="icon1"></span>Dashboard</a></li>
<li><a href="category_management.php"><span class="icon22"></span>Category Management</a></li>
<li><a href="admin.php"><span class="icon2"></span>Channel Management</a></li>
<li><a href="device_management.php"><span class="icon3"></span>Device Management</a></li>
<li><a href="attach_device.php"><span class="icon4"></span>Device Activation</a></li>
<li><a href="content_management.php"><span class="icon5"></span>About Us Page</a></li>
<li><a href="get_streams.php"><span class="icon7"></span>Get Streams</a></li>
<li><a href="test_streams.php"><span class="icon8"></span>Test Streams</a></li>
<li><a href="#"><span class="icon6"></span>Backup DB</a></li>
<!--<li><a href="#"><span class="icon5"></span>Settings</a></li>
<li><a href="#"><span class="icon9"></span>Users</a></li>
<li><a href="#"><span class="icon10"></span>Upload</a></li>
<li><a href="#"><span class="icon13"></span>Download</a></li>
<li><a href="#"><span class="icon11"></span>Plus</a></li>
<li><a href="#"><span class="icon12"></span>Add product</a></li>
<li><a href="#"><span class="icon14"></span>Photos</a></li>
<li><a href="#"><span class="icon15"></span>Comments</a></li>
<li><a href="#"><span class="icon17"></span>New mail</a></li>
<li><a href="#"><span class="icon16"></span>Database</a></li>
<li><a href="#"><span class="icon18"></span>Favorites</a></li>
<li><a href="#"><span class="icon19"></span>Security settings</a></li>
<li><a href="#"><span class="icon20"></span>New page</a></li>-->
</ul>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
</section><!-- End of #second_top_bar -->
				</section><!-- End of .top_in -->
				
			</section><!-- End of #top -->
<?php }?>