<?php 
include 'head.php';
if(isset($_SESSION['user_info'])){
$error=NULL;
$case=1;
$case1=1;
if(isset($_POST['save']))
{ 
if($_POST['category_name']=='' || $_POST['category_name']==NULL)
{
	$case=0;
	$error="<li>Please Enter Category Name</li>";
}
if($_FILES['channel_logo']['name']=='' || $_FILES['channel_logo']['name']==NULL)
{
	$case=0;
	$error.="<li>Please Choose Channel Logo</li>";
}
	
if($case==1){
	
	$name=$_FILES['channel_logo']['name'] ;
	$extract=explode(".", $name);
	$extension=$extract['1'];
	if($name!=''){
if($extension=="jpg" || $extension=="JPG" || $extension=="png"){
$case=1;
$case1=1;
}
else{
$case=0;
$case1=0;
$error='<li>Image formate not correct</li>'; 
}}	
if($case==1 && $case1==1){
	$final_name=time().".".$extension;
$uploads_dir = 'channel_images';
$tmp_name = $_FILES["channel_logo"]["tmp_name"];
$name = $final_name;
move_uploaded_file($tmp_name, "$uploads_dir/$name");
$con = new db_connect();
$auth = new Select_DB();
$connection=$con->connect();
if($connection==1){
 $insert = new Select_DB();
$connection=$insert->insert_category($_POST['parent_category_id'],$_POST['category_name'],$name,"tbl_category");
echo "<script>location.href='category_management.php'</script>";
}
}
}}
?>
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                  
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="<?php echo SITE_URL; ?>logout.php" class="mb-control" data-box="#mb-signout" style="width:100px"><span class="fa fa-sign-out"></span> Logout</a>
                    </li> 
                    <!-- END SIGN OUT -->
                 
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                   
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard </li>
                    <li><a href="#">Categories</a></li>
                    <li class="active">Add Category</li>
                </ul>
                <!-- END BREADCRUMB -->             
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                <?php if($case==0){?>
					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<strong>ERROR!</strong>
						<?php echo $error; ?>
					</div>
					<?php } ?>
						<div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add</strong> Category</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                </div>
                                <div class="panel-body">              
								
									<?php 
										 $con = new db_connect();
										$auth = new Select_DB();
										$connection=$con->connect();
										if($connection==1){
										$get = new Select_DB();
										$result_cats=$get->get_categories("tbl_category",0);
										}
										
									?>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Parent Category</label>
                                        <div class="col-md-6 col-xs-12">                                                                                           
                                            <select class="form-control select" name="parent_category_id">
												<option value="0">(None)</option>
                                                <?php	
													while($row_cat=mysql_fetch_array($result_cats))
													{
												?>
												<option  value="<?php echo $row_cat['category_id']; ?>"><?php echo $row_cat['category_name'];?></option> 
												<?php } ?>
                                            </select>
                                            <span class="help-block">Select Parent Category</span>
                                        </div>
                                    </div>                                                          
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Category Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="category_name" class="form-control"/>
                                            </div>                                            
                                            <span class="help-block">Enter Category Name</span>
                                        </div>
                                    </div>
                                    
									<div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Category Logo</label>
                                        <div class="col-md-6 col-xs-12">            
                                            <input type="file" class="fileinput btn-primary" name="channel_logo" id="filename" title="Browse file"/>
                                            <span class="help-block">Upload Category Logo</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <input type="reset" class="btn btn-default" value="Clear Form" />                                    
                                    <input type="submit" name="save" class="btn btn-primary pull-right" value="Submit" />
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                    </div>
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="logout.php" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>                
        <!-- END PLUGINS -->
        
        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <!-- END THIS PAGE PLUGINS -->       
        
        <!-- START TEMPLATE -->

        
        <script type="text/javascript" src="js/plugins.js"></script>        
        <script type="text/javascript" src="js/actions.js"></script>        
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->
<script>
	$(document).ready(function(){
		$("ul.categories li:nth-child(2)").addClass("active");
		$("#categories_li").addClass("active");
	});
	</script>	
    </body>
</html>
<?php }else{
	
	echo "You are not authorized to visit this page direclty,Sorry";
	} ?>




