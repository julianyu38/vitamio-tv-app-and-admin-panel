<?php 
include 'head.php';
if(isset($_SESSION['user_info'])){
$error=NULL;
$case=1;
$case1=1;
$con = new db_connect();
$auth = new Select_DB();
$connection=$con->connect();
if(isset($_POST['save']))
{ 
	if($_POST['full_name']=='' || $_POST['full_name']==NULL)
	{
		$case=0;
		$error="<li>Please Enter Reseller Full Name</li>";
		
	}
	
	if(($_POST['username']=='' || $_POST['username']==NULL) && $case !=0)
	{
		$case=0;
		$error="<li>Please Enter Reseller Username</li>";
	}
	if(($_POST['password']=='' || $_POST['password']==NULL) && $case !=0)
	{
		$case=0;
		$error="<li>Please Enter Reseller Password</li>";
	}
	if((@$_POST['status']=='' || @$_POST['status']==NULL) && $case !=0)
	{
		$case=0;
		$error="<li>Please Enter Select Reseller Status</li>";
	}
	
	
if($case==1 && $case1==1){

	if($connection==1){
		$insert = new Select_DB();
		$connection=$insert->updateReseller($_GET['id'], $_POST['client_name'],$_POST['full_name'],$_POST['username'],$_POST['password'],$_POST['userLimit'],$_POST['status'],"tbl_user");
		
		echo "<script>location.href='reseller_management.php'</script>";
	}
}
}
?>
<!-- PAGE CONTENT -->

<div class="page-content"> 
  
  <!-- START X-NAVIGATION VERTICAL -->
  <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
    <!-- TOGGLE NAVIGATION -->
    <li class="xn-icon-button"> <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a> </li>
    <!-- END TOGGLE NAVIGATION --> 
    <!-- SIGN OUT -->
    <li class="xn-icon-button pull-right"> <a href="<?php echo SITE_URL; ?>logout.php" class="mb-control" data-box="#mb-signout" style="width:100px"><span class="fa fa-sign-out"></span> Logout</a> </li>
    <!-- END SIGN OUT -->
    
  </ul>
  <!-- END X-NAVIGATION VERTICAL --> 
  
  <!-- START BREADCRUMB -->
  <ul class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Reseller Management</a></li>
    <li class="active">Edit Reseller</li>
  </ul>
  <!-- END BREADCRUMB --> 
  
  <!-- PAGE TITLE -->
  <div class="page-title">
    <h2><span class="fa fa-arrow-circle-o-left"></span> Reseller Management</h2>
  </div>
  <!-- END PAGE TITLE --> 
  
  <!-- PAGE CONTENT WRAPPER -->
  <div class="page-content-wrap">
    <?php if($case==0){?>
    <div class="alert alert-danger" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      <strong>ERROR!</strong> <?php echo $error; ?> </div>
    <?php } ?>
    <?php
	if($connection==1){
		$editReseller = new Select_DB();
		$result = $editReseller->editReseller($_GET['id']);
		$rows = mysql_fetch_array($result, MYSQL_ASSOC);
		//print_r($rows);
	}
	?>
    <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal" method="post" enctype="multipart/form-data">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title"><strong>Edit</strong> Reseller</h3>
              <ul class="panel-controls">
                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
              </ul>
            </div>
            <div class="panel-body"> </div>
            <div class="panel-body">
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Client Type</label>
                <div class="col-md-6 col-xs-12">
                <?php
				$check = ($rows['user_type'] == 2) ? 'selected="selected"' : '';
				?>
                  <select class="form-control select" name="client_name">
                    <option value="2" <?php echo $check; ?> >Reseller</option>
                  </select>
                  <span class="help-block">Select User Type</span> </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Reseller Full Name</label>
                <div class="col-md-6 col-xs-12">
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="full_name" class="form-control" value="<?php echo $rows['user_full_name']; ?>" />
                  </div>
                  <span class="help-block">Enter Reseller Full Name</span> </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Username</label>
                <div class="col-md-6 col-xs-12">
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-user"></span></span>
                    <input type="text" name="username" class="form-control" value="<?php echo $rows['user_name']; ?>" />
                  </div>
                  <span class="help-block">Enter Reseller Username</span> </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Reseller Password</label>
                <div class="col-md-6 col-xs-12">
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-key"></span></span>
                    <input type="text" name="password" class="form-control" value="<?php echo $rows['password']; ?>" />
                  </div>
                  <span class="help-block">Enter Reseller Password</span> </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Reseller User Limit</label>
                <div class="col-md-6 col-xs-12">
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-list"></span></span>
                    <input type="text" name="userLimit" class="form-control" value="<?php echo $rows['reseller_user_limit']; ?>" />
                  </div>
                  <span class="help-block">Enter Reseller User Limit</span> </div>
              </div>
              <?php
			  $active = ($rows['status'] == 1) ? 'checked="checked"' : '';
			  $inActive = ($rows['status'] == 2) ? 'checked="checked"' : '';
			  ?>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Reseller Status</label>
                <div class="col-md-6 col-xs-12">
                  <p>
                    <label>
                      <input type="radio" name="status" value="1" id="status_0" <?php echo $active; ?> />
                      Active</label>
                    <br />
                    <label>
                      <input type="radio" name="status" value="2" id="status_1" <?php echo $inActive; ?> />
                      InActive</label>
                    <br />
                  </p>
                </div>
              </div>
            </div>
            <div class="panel-footer">
              <input type="reset" class="btn btn-default" value="Clear Form" />
              <input type="submit" name="save" class="btn btn-primary pull-right" value="Submit" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT --> 

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
  <div class="mb-container">
    <div class="mb-middle">
      <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
      <div class="mb-content">
        <p>Are you sure you want to log out?</p>
        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
      </div>
      <div class="mb-footer">
        <div class="pull-right"> <a href="logout.php" class="btn btn-success btn-lg">Yes</a>
          <button class="btn btn-default btn-lg mb-control-close">No</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MESSAGE BOX--> 

<!-- START PRELOADS -->
<audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS --> 

<!-- START SCRIPTS --> 
<!-- START PLUGINS --> 
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script> 
<!-- END PLUGINS --> 

<!-- THIS PAGE PLUGINS --> 
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script> 
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script> 
<script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script> 
<!-- END THIS PAGE PLUGINS --> 

<!-- START TEMPLATE --> 
<script type="text/javascript" src="js/plugins.js"></script> 
<script type="text/javascript" src="js/actions.js"></script> 
<!-- END TEMPLATE --> 
<!-- END SCRIPTS -->
</body></html><?php }else{
	
	echo "You are not authorized to visit this page direclty,Sorry";
	} ?>
