<?php 
include 'head.php';
if(isset($_SESSION['user_info'])){
?>
<!-- PAGE CONTENT -->

<div class="page-content">
<?php
$con = new db_connect();
$auth = new Select_DB();
$connection=$con->connect();
if($connection==1){
	$get = new Select_DB();
	$and = ($_SESSION['user_info']['user_type'] != 1) ? " WHERE user_id_fk = " . $_SESSION['user_info']['user_id'] : "";
	$result=$get->select_channel("tbl_device $and");
	//$count=count($rows);
	//print_r($rows);
}
?>
  <!-- START X-NAVIGATION VERTICAL -->
  <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
    <!-- TOGGLE NAVIGATION -->
    <li class="xn-icon-button"> <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a> </li>
    <!-- END TOGGLE NAVIGATION --> 
    <!-- SEARCH -->
    <li class="xn-search">
      <form role="form">
        <input type="text" name="search" placeholder="Search...">
      </form>
    </li>
    <!-- END SEARCH --> 
    <!-- SIGN OUT -->
    <li class="xn-icon-button pull-right"> <a href="<?php echo SITE_URL; ?>logout.php" class="mb-control" data-box="#mb-signout" style="width:100px"><span class="fa fa-sign-out"></span> Logout</a> </li>
    <!-- END SIGN OUT -->
    
  </ul>
  <!-- END X-NAVIGATION VERTICAL --> 
  
  <!-- START BREADCRUMB -->
  <ul class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Clients Management</a></li>
    <li class="active">Clients</li>
  </ul>
  <!-- END BREADCRUMB --> 
  
  <!-- PAGE TITLE -->
  <div class="page-title">
    <h2><span class="fa fa-arrow-circle-o-left"></span>Clients Management</h2>
  </div>
  <!-- END PAGE TITLE --> 
  
  <!-- PAGE CONTENT WRAPPER -->
  <div class="page-content-wrap">
    <div class="row">
      <div class="col-md-12"> 
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Clients Management Here</h3>
            <ul class="panel-controls">
              <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
              <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
              <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
            </ul>
          </div>
          <div class="panel-body">
            <table class="table datatable">
              <thead>
                <tr>
                  <th>Client Nr</th>
                  <th>Device Name</th>
                  <th>Device ID Number</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <!--<th>Channel Attached</th>
												<th>Channel Logo</th>
												<th>Channel Stream</th>
												<th>Device Attached</th>-->
                  <?php
				  if($_SESSION['user_info']['user_type'] == 1 && !empty($_SESSION)
					&& $_SESSION['user_info']['user_id'] == 1){?>
                  <th>Actions</th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>
                <?php 
											$no =1;
											while($row=mysql_fetch_array($result)){ 
											?>
                <tr>
                  <td ><?php echo $no; $no++; ?></td>
                  <td style="cursor:pointer;"><a style="color:#000;" href="channel_list.php?device_name=<?php echo $row['device_mac_address'];  ?>" ><?php echo $row['device_name']; ?></a>
<span class="help-block"><?php echo $auth->reseller_or_user($row['user_id_fk']); ?></span>
</td>
                  <td style="cursor:pointer;"><a style="color:#000;" href="channel_list.php?device_name=<?php echo $row['device_mac_address'];  ?>" ><?php echo $row['device_mac_address']; ?></a></td>
                  <td style="cursor:pointer;"><a style="color:#000;" href="channel_list.php?device_name=<?php echo $row['device_mac_address'];  ?>" ><?php echo $row['start_date']; ?></a></td>
                  <td style="cursor:pointer;"><a style="color:#000;" href="channel_list.php?device_name=<?php echo $row['device_mac_address'];  ?>" ><?php echo $row['end_date']; ?></a></td>
                  <!--<td><?php
											//$phrase = $row['channel_description'];
											//echo implode(' ', array_slice(str_word_count($phrase, 2), 0, 5));?></td>
											<td align="center"><img width="92px;" height="75px;" src="channel_images/<?php //echo $row['channel_logo'];  ?>" style="border:solid 1px #CCC; margin-left: 75px; border-radius: 5px 5px 5px 5px;"></td>
											<td><?php //echo $row['channel_stream']; ?></td>
											<td><?php //if($row['device_id']==0){echo "No Device Attached";} else{ echo $row['deviced_attached'];} ?></td>-->
                  <?php
				  if($_SESSION['user_info']['user_type'] == 1 && !empty($_SESSION)
					&& $_SESSION['user_info']['user_id'] == 1){?>
                  <td>
                  <a href="add_device.php">
                    <button class="btn btn-default btn-rounded btn-sm"><span class="fa fa-plus"></span></button>
                    </a>
                  <a href="edit_device.php?device_name=<?php echo $row['device_mac_address'];  ?>&id=<?php echo $row['device_id'];  ?>">
                    <button class="btn btn-default btn-rounded btn-sm"><span class="fa fa-pencil"></span></button>
                    </a>
				  <a href="delete_device.php?device_name=<?php echo $row['device_mac_address'];  ?>&id=<?php echo $row['device_id'];  ?>" onclick="return confirm('Are you sure want to delete');">
                    <button class="btn btn-danger btn-rounded btn-sm"><span class="fa fa-times"></span></button>
                    </a></td>
				  <?php } ?>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END DEFAULT DATATABLE --> 
      </div>
    </div>
  </div>
  <!-- PAGE CONTENT WRAPPER --> 
  
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER --> 

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
  <div class="mb-container">
    <div class="mb-middle">
      <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
      <div class="mb-content">
        <p>Are you sure you want to log out?</p>
        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
      </div>
      <div class="mb-footer">
        <div class="pull-right"> <a href="logout.php" class="btn btn-success btn-lg">Yes</a>
          <button class="btn btn-default btn-lg mb-control-close">No</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MESSAGE BOX--> 

<!-- START PRELOADS -->
<audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS --> 

<!-- START SCRIPTS --> 
<!-- START PLUGINS --> 
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script> 
<!-- END PLUGINS --> 

<!-- START THIS PAGE PLUGINS--> 
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script> 
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script> 
<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script> 
<!-- END PAGE PLUGINS --> 

<!-- START TEMPLATE --> 
<script type="text/javascript" src="js/plugins.js"></script> 
<script type="text/javascript" src="js/actions.js"></script> 
<!-- END SCRIPTS --> 
<script>
	$(document).ready(function(){
		$("ul.devices li:first-child").addClass("active");
		$("#devices_li").addClass("active");
	});
	</script>
</body></html><?php }else{
	
	echo "You are not authorized to visit this page direclty,Sorry";
} ?>