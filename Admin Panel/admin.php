<?php 
include 'head.php';
if(isset($_SESSION['user_info'])){
	//print_r( $_SESSION['user_info'] );exit;

$con = new db_connect();
$auth = new Select_DB();
$connection=$con->connect();
if($connection==1){
 $get = new Select_DB();
 $total_channels=$get->count_tbls("tbl_channel");
 
 $and = ($_SESSION['user_info']['user_type'] != 1) ? " WHERE user_id_fk = " . $_SESSION['user_info']['user_id'] : "";
 $total_devices=$get->count_tbls("tbl_device $and");
 $result=$get->select_channel("tbl_device");
 $result1=$get->select_channel("tbl_channel");
}
?>

<!-- PAGE CONTENT -->

<div class="page-content"> 
  
  <!-- START X-NAVIGATION VERTICAL -->
  <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
    <!-- TOGGLE NAVIGATION -->
    <li class="xn-icon-button"> <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a> </li>
    <!-- END TOGGLE NAVIGATION --> 
    <!-- SEARCH -->
    
    </li>
    <!-- END SEARCH --> 
    <!-- SIGN OUT -->
    <li class="xn-icon-button pull-right"> <a href="<?php echo SITE_URL; ?>logout.php" class="mb-control" data-box="#mb-signout" style="width:100px"><span class="fa fa-sign-out"></span> Logout</a> </li>
    <!-- END SIGN OUT -->
    
  </ul>
  <!-- END X-NAVIGATION VERTICAL --> 
  
  <!-- START BREADCRUMB -->
  <ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li class="active">Dashboard</li>
  </ul>
  <!-- END BREADCRUMB --> 
  
  <!-- PAGE CONTENT WRAPPER -->
  <div class="page-content-wrap"> 
    
    <!-- START WIDGETS -->
    <div class="row">
      <div class="col-md-3"> 
        
        <!-- START WIDGET SLIDER -->
        <div class="widget widget-default widget-carousel">
          <div class="owl-carousel" id="owl-example">
            <div>
              <div class="widget-title">Total Visitors</div>
              <?php $handle = fopen("counter.txt", "r");
			  if(!$handle){ 
				  echo "could not open the file"; 
			  } else { 
				  $counter=(int )fread($handle,20); 
				  fclose($handle); 
				  $counter++;
				  echo" <div class='widget-int'> ". $counter . " </div > " ; 
				  $handle= fopen("counter.txt", "w" );
				  fwrite($handle,$counter) ; 
				  fclose ($handle) ; 
			  } ?>
            </div>
            <!--<div>                                    
                    <div class="widget-title">Returned</div>
                    <div class="widget-subtitle">Visitors</div>
                    <div class="widget-int">1,695</div>
                </div>
                <div>                                    
                    <div class="widget-title">New</div>
                    <div class="widget-subtitle">Visitors</div>
                    <div class="widget-int">1,977</div>
                </div>--> 
          </div>
          <div class="widget-controls"> <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a> </div>
        </div>
        <!-- END WIDGET SLIDER --> 
        
      </div>
      <div class="col-md-3"> 
        
        <!-- START WIDGET Channel Numers--> 
        <!--<div class="widget widget-default widget-item-icon" onclick="location.href='../channels.php';">-->
        <?php
		$channelLink = ($_SESSION['user_info']['user_type'] == 1) ? "location.href='".SITE_URL."channels.php';": "";
		?>
        <div class="widget widget-default widget-item-icon" onclick="<?php echo $channelLink;?>" style="cursor:pointer;">
          <div class="widget-item-left"> <span class="glyphicon glyphicon-play-circle"></span> </div>
          <div class="widget-data">
            <div class="widget-int num-count"><?php echo $total_channels; ?></div>
            <div class="widget-title">Channel Numbers</div>
          </div>
          <div class="widget-controls"> <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a> </div>
        </div>
        <!-- END WIDGET Channel Numers --> 
        
      </div>
      <div class="col-md-3"> 
        
        <!-- START WIDGET USERS -->
        <div class="widget widget-default widget-item-icon">
          <div class="widget-item-left"> <span class="fa fa-user"></span> </div>
          <div class="widget-data">
            <div class="widget-int num-count"><?php echo $total_devices; ?></div>
            <div class="widget-title">CLIENTS</div>
            <div class="widget-subtitle">IPTV Devices</div>
          </div>
          <div class="widget-controls"> <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a> </div>
        </div>
        <!-- END WIDGET USERS --> 
        
      </div>
      <div class="col-md-3"> 
        
        <!-- START WIDGET CLOCK -->
        <div class="widget widget-danger widget-padding-sm">
          <div class="widget-big-int plugin-clock">00:00</div>
          <div class="widget-subtitle plugin-date">Loading...</div>
          <div class="widget-controls"> <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a> </div>
          <div class="widget-buttons widget-c3">
            <div class="col"> <a href="#"><span class="fa fa-clock-o"></span></a> </div>
            <div class="col"> <a href="#"><span class="fa fa-bell"></span></a> </div>
            <div class="col"> <a href="#"><span class="fa fa-calendar"></span></a> </div>
          </div>
        </div>
        <!-- END WIDGET CLOCK --> 
        
      </div>
    </div>
    <!-- END WIDGETS -->
    
    <div class="row">
      <div class="col-md-12"> 
      <?php
	  if($_SESSION['user_info']['user_type'] != 1){
	  ?>
      <!-- User Limit Section -->
        <div class="alert alert-info" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      <strong>Remain</strong> <?php
      
	  $limitQry_1 = mysql_query("SELECT * FROM tbl_user WHERE user_id = ".$_SESSION['user_info']['user_id']."")
			or die( mysql_error() );
$countLimit1 = mysql_fetch_array( $limitQry_1, MYSQL_ASSOC );
//print_r($countLimit1);
$limitQry = mysql_query("SELECT COUNT(device_id) AS userCount FROM tbl_device WHERE user_id_fk = '".$_SESSION['user_info']['user_id']."'")
			or die( mysql_error() );
$countLimit = mysql_fetch_array( $limitQry );

# RESELLER'S RESELLER USER LIMIT
$rSeller = mysql_query("SELECT * FROM `tbl_user` WHERE `user_id_fk` = '".$_SESSION['user_info']['user_id']."' AND `reseller_user_limit` IS NOT NULL AND `reseller_user_limit` !=0") or die( mysql_error() );
$moreReseller = 0;
while( $rSellerLimit = mysql_fetch_array($rSeller) ){
	$moreReseller += $rSellerLimit['seller_user_limit'];
}

if($countLimit1['reseller_user_limit'] >= $countLimit['userCount'] ){
	echo 'Users Left : ' . ($countLimit1['reseller_user_limit'] - $countLimit['userCount'] - $moreReseller); 
}
	  
	  ?> </div>
        <!-- User Limit Section -->
        <?php } ?>
        
        <!-- START CHANNELS TABLE -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Clients</h3>
          </div>
          <div class="panel-body">
            <table class="table datatable">
              <thead>
                <tr>
                  <th>Client Nr</th>
                  <th>Device Name</th>
                  <th>Device ID Number</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <?php
				  if($_SESSION['user_info']['user_type'] == 1 && !empty($_SESSION)
					&& $_SESSION['user_info']['user_id'] == 1){?>
                  <th>Actions</th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>
                <?php 
				//$user_id_fk = $_SESSION['user_info']['user_id'];
				$and = ($_SESSION['user_info']['user_type'] != 1) ? " WHERE user_id_fk = " . $_SESSION['user_info']['user_id'] : "";
				$result=$get->select_channel("tbl_device $and");
				$no =1;
				while($row=mysql_fetch_array($result)){ 
				?>
                <tr>
                  <td ><?php echo $no; $no++; ?></td>
                  <td style="cursor:pointer;"><a style="color:#000;" href="channel_list.php?device_name=<?php echo $row['device_mac_address'];  ?>" ><?php echo $row['device_name']; ?></a><br />
                    <span class="help-block"><?php echo $auth->reseller_or_user($row['user_id_fk']); ?></span></td>
                  <td style="cursor:pointer;"><a style="color:#000;" href="channel_list.php?device_name=<?php echo $row['device_mac_address'];  ?>" ><?php echo $row['device_mac_address']; ?></a></td>
                  <td style="cursor:pointer;"><a style="color:#000;" href="channel_list.php?device_name=<?php echo $row['device_mac_address'];  ?>" ><?php echo $row['start_date']; ?></a></td>
                  <td style="cursor:pointer;"><a style="color:#000;" href="channel_list.php?device_name=<?php echo $row['device_mac_address'];  ?>" ><?php echo $row['end_date']; ?></a></td>
                  <?php
				  if($_SESSION['user_info']['user_type'] == 1 && !empty($_SESSION)
					&& $_SESSION['user_info']['user_id'] == 1){?>
                  <td><a href="add_device.php">
                    <button class="btn btn-default btn-rounded btn-sm"><span class="fa fa-plus"></span></button>
                    </a> <a href="edit_device.php?device_name=<?php echo $row['device_mac_address'];  ?>&id=<?php echo $row['device_id'];  ?>">
                    <button class="btn btn-default btn-rounded btn-sm"><span class="fa fa-pencil"></span></button>
                    </a> <a href="delete_device.php?device_name=<?php echo $row['device_mac_address'];  ?>&id=<?php echo $row['device_id'];  ?>" onclick="return confirm('Are you sure want to delete');">
                    <button class="btn btn-danger btn-rounded btn-sm"><span class="fa fa-times"></span></button>
                    </a></td>
                  <?php } ?>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END CHANNELS TABLE--> 
      </div>
    </div>
  </div>
  <!-- END PAGE CONTENT WRAPPER -->
  <footer>
    <p>All Copy &copy; Reserved</p>
  </footer>
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER --> 
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
  <div class="mb-container">
    <div class="mb-middle">
      <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
      <div class="mb-content">
        <p>Are you sure you want to log out?</p>
        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
      </div>
      <div class="mb-footer">
        <div class="pull-right"> <a href="logout.php" class="btn btn-success btn-lg">Yes</a>
          <button class="btn btn-default btn-lg mb-control-close">No</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MESSAGE BOX--> 

<!-- START PRELOADS -->
<audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS --> 

<!-- START SCRIPTS --> 
<!-- START PLUGINS --> 
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script> 
<!-- END PLUGINS --> 

<!-- START THIS PAGE PLUGINS--> 
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script> 
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script> 
<script type="text/javascript" src="js/plugins/scrolltotop/scrolltopcontrol.js"></script> 
<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="js/plugins/morris/raphael-min.js"></script> 
<script type="text/javascript" src="js/plugins/morris/morris.min.js"></script> 
<script type="text/javascript" src="js/plugins/rickshaw/d3.v3.js"></script> 
<script type="text/javascript" src="js/plugins/rickshaw/rickshaw.min.js"></script> 
<script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script> 
<script type='text/javascript' src='js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script> 
<script type='text/javascript' src='js/plugins/bootstrap/bootstrap-datepicker.js'></script> 
<script type="text/javascript" src="js/plugins/owl/owl.carousel.min.js"></script> 
<script type="text/javascript" src="js/plugins/moment.min.js"></script> 
<script type="text/javascript" src="js/plugins/daterangepicker/daterangepicker.js"></script> 
<!-- END THIS PAGE PLUGINS--> 

<!-- START TEMPLATE --> 

<script type="text/javascript" src="js/plugins.js"></script> 
<script type="text/javascript" src="js/actions.js"></script> 
<script type="text/javascript" src="js/demo_dashboard.js"></script> 
<script>
$(document).ready(function(){
$("#dashboard").addClass("active");
});
</script> 
<!-- END TEMPLATE --> 
<!-- END SCRIPTS -->
</body></html><?php }else{
	
	echo "You are not authorized to visit this page direclty,Sorry";
	} ?>
