<?php 
include 'head.php';
if(isset($_SESSION['user_info'])){
$error=NULL;
$case=1;
$case1=1;
if(isset($_POST['save']))
{ 
if($_POST['mac_address']=='' || $_POST['mac_address']==NULL)
{
	$case=0;
	$error="<li>Please Enter Device ID Nr</li>";
	
}

if(($_POST['d_name']=='' || $_POST['d_name']==NULL) && $case !=0)
{
	$case=0;
	$error="<li>Please Enter Device Name</li>";
}
 if(($_POST['start_date']=='' || $_POST['start_date']==NULL ) && $case !=0)
{
	$case=0;
	$error="<li>Please Enter Start Date</li>";
}
if(($_POST['end_date']=='' || $_POST['end_date']==NULL) && $case !=0 )
{
	$case=0;
	$error="<li>Please Enter End Date</li>";
} 

//echo  $_POST['start_date'];
	
	
if($case==1 && $case1==1){
	$con = new db_connect();
	$auth = new Select_DB();
	$connection=$con->connect();
	
	if($connection==1){
		$insert = new Select_DB();
		$connection=$insert->insert_device($_POST['mac_address'],$_POST['d_name'],$_POST['start_date'],$_POST['end_date'],"tbl_device");
		
		//$insert->addUser($_POST['d_name'], $_POST['client_name']);
	
		echo "<script>location.href='device_management.php'</script>";
	}
}
}
?>
<!-- PAGE CONTENT -->

<div class="page-content"> 
  
  <!-- START X-NAVIGATION VERTICAL -->
  <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
    <!-- TOGGLE NAVIGATION -->
    <li class="xn-icon-button"> <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a> </li>
    <!-- END TOGGLE NAVIGATION --> 
    
    <!-- SIGN OUT -->
    <li class="xn-icon-button pull-right"> <a href="<?php echo SITE_URL; ?>logout.php" class="mb-control" data-box="#mb-signout" style="width:100px"><span class="fa fa-sign-out"></span> Logout</a> </li>
    <!-- END SIGN OUT -->
    
  </ul>
  <!-- END X-NAVIGATION VERTICAL --> 
  
  <!-- START BREADCRUMB -->
  <ul class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Clients</a></li>
    <li class="active">Add Client</li>
  </ul>
  <!-- END BREADCRUMB --> 
  
  <!-- PAGE CONTENT WRAPPER -->
  <div class="page-content-wrap">
    <?php if($case==0){?>
    <div class="alert alert-danger" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      <strong>ERROR!</strong> <?php echo $error; ?> </div>
    <?php } ?>
    <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal" enctype="multipart/form-data" method="post">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title"><strong>Add</strong> Client</h3>
              <ul class="panel-controls">
                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
              </ul>
            </div>
            <div class="panel-body"> </div>
            <div class="panel-body">
              <?php
              /*
              if($_SESSION['user_info']['user_type'] == 1 && !empty($_SESSION)
			  	&& $_SESSION['user_info']['user_id'] == 1){
              ?>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Client Type</label>
                <div class="col-md-6 col-xs-12">
                  <select class="form-control select" name="client_name">
                    <!--<option  value="1">Admin</option>-->
                    <option  value="2">Reseller</option>
                    <option  value="3" selected="selected">User</option>
                  </select>
                  <span class="help-block">Select User Type</span> </div>
              </div>
              <?php } */?>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Custom Device ID</label>
                <button onclick="bootstro.start('.bootstro');" class="btn btn-danger">How to get device ID</button>
                <div class="col-md-6 col-xs-12">
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="mac_address" class="form-control"/>
                  </div>
                  <span class="help-block">Enter Custom Device ID</span> </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Client Name</label>
                <div class="col-md-6 col-xs-12">
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                    <input type="text" name="d_name" class="form-control"/>
                  </div>
                  <span class="help-block">Enter Client Name</span> </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Start Date</label>
                <div class="col-md-6 col-xs-12">
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                    <input type="text" class="form-control datepicker" name="start_date" value="<?php echo date("Y-m-d"); ?>">
                  </div>
                  <span class="help-block">Pick The Start Date</span> </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">End Date</label>
                <div class="col-md-6 col-xs-12">
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                    <input type="text" class="form-control datepicker" name="end_date" value="<?php echo date("Y-m-d"); ?>">
                  </div>
                  <span class="help-block">Pick The End Date</span> </div>
              </div>
            </div>
            <div class="panel-footer">
              <input type="reset" class="btn btn-default" value="Clear Form" />
              <input type="submit" name="save" class="btn btn-primary pull-right" value="Submit" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT --> 

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
  <div class="mb-container">
    <div class="mb-middle">
      <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
      <div class="mb-content">
        <p>Are you sure you want to log out?</p>
        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
      </div>
      <div class="mb-footer">
        <div class="pull-right"> <a href="logout.php" class="btn btn-success btn-lg">Yes</a>
          <button class="btn btn-default btn-lg mb-control-close">No</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MESSAGE BOX--> 

<!-- START PRELOADS -->
<audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS --> 

<!-- START SCRIPTS --> 
<!-- START PLUGINS --> 
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script> 
<!-- END PLUGINS --> 

<!-- THIS PAGE PLUGINS --> 
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script> 
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script> 
<script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script> 
<!-- END THIS PAGE PLUGINS --> 

<!-- START TEMPLATE --> 

<script type="text/javascript" src="js/plugins.js"></script> 
<script type="text/javascript" src="js/actions.js"></script> 
<!-- END TEMPLATE --> 
<!-- END SCRIPTS --> 
<script>
	$(document).ready(function(){
		$("ul.devices li:nth-child(2)").addClass("active");
		$("#devices_li").addClass("active");
	});
	</script>
</body></html><?php }else{
	
	echo "You are not authorized to visit this page direclty,Sorry";
	} ?>
