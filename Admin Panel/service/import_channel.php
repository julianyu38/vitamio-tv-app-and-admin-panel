<?php
include 'head.php';
if(isset($_SESSION['user_info'])){

	$step=1;
	if(isset($_REQUEST['step']))
	{
		$step=$_REQUEST['step'];
	}	
		
	$error=NULL;
	$case=1;
	if(isset($_POST['step']) && $_POST['step']==2)
	{
		$m3u8_content=trim($_POST['m3u8_content']);
		if($m3u8_content=='')
		{
			$case=0;
			$step=1;
			$error="<li>Please enter m3u8 file content</li>";
		}
		if($case==1)
		{
			$tmp_channels = explode("\n", $m3u8_content); // make an array out of curl return value
			//unset($pieces[0]); // remove #EXTM3U
			$tmp_channels = array_map('trim', $tmp_channels); // remove unnecessary space
			$tmp_channels = array_filter($tmp_channels); // remove empty lines
			$tmp_channels = array_chunk($tmp_channels, 2); // group them by two's
			$channels=array();
			foreach($tmp_channels AS $channel)
			{
				if(count($channel)>=2)
				{
					$channel1=array();
					
					$channel_line1=trim($channel[0]);
					$channel_line2=trim($channel[1]);
					
					$channel_name=explode(",",$channel_line1);
					$channel_name=trim($channel_name[1]);
					
					$channel1['name']=$channel_name;
					$channel1['url']=$channel_line2;
					
					$channels[]=$channel1;
				}
			}
			if(count($channels)<=0)
			{
				$case=0;
				$step=1;
				$error="<li>Failed to parse your m3u8 file content</li>";
			}
		}
	}
	else if(isset($_POST['step']) && $_POST['step']==3)
	{
		$cntImport=0;
		for($i=0;$i<count($_POST['channel_name']);$i++)
		{
			if($_POST['channel_name'][$i]=='' || $_POST['channel_name'][$i]==NULL)
			{
				$case=0;
			}
			if($_POST['channel_stream'][$i]=='' || $_POST['channel_stream'][$i]==NULL)
			{
				$case=0;
			}
			if($case==1){
				$con = new db_connect();
				$auth = new Select_DB();
				$connection=$con->connect();
				if($connection==1){
					$insert = new Select_DB();
					
					$channel_name=$_POST['channel_name'][$i];
					$channel_description=$_POST['channel_description'][$i];
					$channel_description="";
					$channel_stream=$_POST['channel_stream'][$i];
					$cat=$_POST['category_name'][$i];
					$subCat=0;
					$catInfoRes=$insert->get_category("tbl_category",$cat);
					$catInfo=mysql_fetch_array($catInfoRes);
					if($catInfo['parent_category_id']>0)
					{
						$cat=$catInfo['parent_category_id'];
						$subCat=$catInfo['category_id'];
					}
					$lookup_file_name=str_replace("-","_",str_replace("=","_",str_replace(" ","_",strtolower($channel_name))));
					$file_name=$lookup_file_name."_".time().".png";
					$uploads_dir = 'channel_images';
					$source_name = $uploads_dir."/default/default.png";
					if(file_exists($uploads_dir."/default/".$lookup_file_name.".png"))
					{
						$source_name = $uploads_dir."/default/".$lookup_file_name.".png";
					}
					@copy($source_name, "$uploads_dir/$file_name");
					
					$connection=$insert->insert_channel($channel_name,$channel_description,$file_name,$channel_stream,$cat,$subCat,"tbl_channel");
					$cntImport++;
				}
			}
		}
		$succ=1;
		$succ_msg="<li>".$cntImport."/".count($_POST['channel_name'])." channels imported successfully.</li>";
		$step=1;
	}
?>
<!-- PAGE CONTENT -->

<div class="page-content"> 
	
	<!-- START X-NAVIGATION VERTICAL -->
	<ul class="x-navigation x-navigation-horizontal x-navigation-panel">
		<!-- TOGGLE NAVIGATION -->
		<li class="xn-icon-button"> <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a> </li>
		<!-- END TOGGLE NAVIGATION --> 
		<!-- SIGN OUT -->
		<li class="xn-icon-button pull-right"> <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a> </li>
		<!-- END SIGN OUT -->
		
	</ul>
	<!-- END X-NAVIGATION VERTICAL --> 
	
	<!-- START BREADCRUMB -->
	<ul class="breadcrumb">
		<li><a href="#">Dashboard></li>
		<li><a href="#">Channels</a></li>
		<li class="active">Import Channels</li>
	</ul>
	<!-- END BREADCRUMB --> 
	
	<!-- PAGE TITLE -->
	<div class="page-title">
		<h2><span class="fa fa-arrow-circle-o-left"></span> Import Channels</h2>
	</div>
	<!-- END PAGE TITLE --> 
	
	<!-- PAGE CONTENT WRAPPER -->
	<div class="page-content-wrap">
		<?php if($case==0){?>
		<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<strong>ERROR!</strong> <?php echo $error; ?> </div>
		<?php }
		else if($succ==1){
			?>
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<strong>SUCCESS!</strong> <?php echo $succ_msg; ?> 
			</div>
			<?php
		}?>
		<div class="row">
			<div class="col-md-12">
				<?php
				if($step==1)
				{
					?>
				<form class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Import</strong> Channels</h3>
							<ul class="panel-controls">
								<li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
							</ul>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-md-3 col-xs-12 control-label">m3u8 content</label>
								<div class="col-md-6 col-xs-12">
									<textarea name="m3u8_content" class="form-control" rows="5"></textarea>
									<span class="help-block">Paste your m3u8 file content here</span> </div>
							</div>
						</div>
						<div class="panel-footer">
							<input type="reset" value="Reset" class="btn btn-default">
							<button type="submit" name="step" value="2" class="btn btn-primary pull-right">Next >></button>
						</div>
					</div>
				</form>
				<?php
				}
				else if($step==2)
				{
					?>
				<form class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Import</strong> Channels</h3>
							<ul class="panel-controls">
								<li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
							</ul>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Stream</th>
										<th>Category</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
								<?php
								foreach($channels AS $channel)
								{
								?>
									<tr>
										<td><input type="text" class="form-control" name="channel_name[]" value="<?=$channel['name']?>"></td>
										<td><input type="text" class="form-control" name="channel_stream[]" value="<?=$channel['url']?>"></td>
										<td>
											<select class="form-control select" name="category_name[]">
                                 	<?php	
													$con = new db_connect();
													$auth = new Select_DB();
													$connection=$con->connect();
													if($connection==1){
														$select = new Select_DB();
														$result=$select->get_categories("tbl_category",0);
													}
													$cntCats=0;
													while($rows12=mysql_fetch_array($result))
													{
														$cntCats++;
													?>
														<option value="<?php echo $rows12['category_id']; ?>" <?=($cntCats==1)?"selected":""?> ><?php echo $rows12['category_name'];?></option> 
														<?php
														$result1=$select->get_categories("tbl_category",$rows12['category_id']);
														while($rows13=mysql_fetch_array($result1))
														{
														?>
															<option value="<?php echo $rows13['category_id']; ?>">&mdash; <?php echo $rows13['category_name'];?></option> 
														<?php
														}
													}
												?>
                              	</select>
										</td>
										<td><a href="javascript:void(0);" class="btn btn-primary" onClick="$(this).parent().parent().remove();"><span class="fa fa-times"></span> Remove</a></td>
									</tr>
								<?php
								}
								?>
								</tbody>
							</table>
						</div>
						<div class="panel-footer">
							<a href="import_channel.php" class="btn btn-default">Back</a>
							<button type="submit" name="step" value="3" class="btn btn-primary pull-right">Import</button>
						</div>
					</div>
				</form>
				<?php
									}
									?>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT --> 

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
			<div class="mb-content">
				<p>Are you sure you want to log out?</p>
				<p>Press No if youwant to continue work. Press Yes to logout current user.</p>
			</div>
			<div class="mb-footer">
				<div class="pull-right"> <a href="logout.php" class="btn btn-success btn-lg">Yes</a>
					<button class="btn btn-default btn-lg mb-control-close">No</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END MESSAGE BOX--> 

<!-- START PRELOADS -->
<audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS --> 

<!-- START SCRIPTS --> 
<!-- START PLUGINS --> 
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script> 
<!-- END PLUGINS --> 

<!-- THIS PAGE PLUGINS --> 
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script> 
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script> 
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script> 
<script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script> 
<!-- END THIS PAGE PLUGINS --> 

<!-- START TEMPLATE --> 

<script type="text/javascript" src="js/plugins.js"></script> 
<script type="text/javascript" src="js/actions.js"></script> 
<!-- END TEMPLATE --> 
<!-- END SCRIPTS --> 
<script>
	$(document).ready(function(){
		$("ul.channels li:nth-child(2)").addClass("active");
		$("#channel_li").addClass("active");
	});
	</script>
</body></html><?php }else{
	
	echo "You are not authorized to visit this page direclty,Sorry";
	} ?>
