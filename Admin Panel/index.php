<?php 
session_start();
include("common/functions.php");
?>
<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>IPTV Admin 1.5 Login Panel</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        <?php 
$error;
$case=1;
$case1=1;
if(isset($_POST['login'])){
$con = new db_connect();
$auth = new Select_DB();
$connection=$con->connect();
$connection;
if($connection==1){
	$login=$auth->autenticate($_POST['login_username'],$_POST['login_password'],"tbl_user");
		if($login==1){
			echo "<script>location.href='admin.php'</script>";
			exit;
		}else{
			$error.=base64_encode('Invalid Username/Password');
			echo "<script>location.href='index.php?error=".$error."'</script>";
			//header("Location:index.php?error=".$error."&user=".$_POST['user_type']."");
			exit;
			}
	}
}
		  
		  
?>	
        <div class="login-container">
		<?php if(isset($_GET['error'])){?>
		<div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <strong>ERROR!</strong>
		<?php echo base64_decode($_GET['error']);  ?>
        </div>
		<?php } ?>
            <div class="login-box animated fadeInDown">
                <h1 style="text-align:center;color:white;font-weight:bold">IPTV Admin Panel 1.5</h1>
                <div class="login-body">
                    <div class="login-title"><strong>Log In</strong> to your account</div>
					<form id="admin_login_form" class="form-3 form-horizontal" method="post" action="">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" required name="login_username" placeholder="User Name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" required name="login_password" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="http://www.iptv-admin.com" class="btn btn-link btn-block">Forgot your password?</a>
                        </div>
                        <div class="col-md-6">
                            <input type="submit" class="btn btn-info btn-block"  name="login" value="Log In"/>
                        </div>
                    </div>
                    <div class="login-or">OR</div>
                 
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2016 IPTV-Admin.com
                    </div>
                    <div class="pull-right">
                        <a href="http://www.iptv-admin.com/">About</a> |
                        <a href="http://www.iptv-admin.com">Privacy</a> |
                        <a href="http://www.iptv-admin.com/contact-us/">Contact Us</a>
                    </div>
                </div>
            </div>
            
        </div>
        
    </body>
</html>





