<?php
session_start();
include("config.php");
if( empty($_SESSION['user_info']) ){
	header("Location: ".SITE_URL);
}
include("common/functions.php");
function curPageName() {
return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
}
function yubi_hex2bin($h)
  {
  if (!is_string($h)) return null;
  $r='';
  for ($a=0; $a<strlen($h); $a+=2) { $r.=chr(hexdec($h{$a}.$h{($a+1)})); }
  return $r;
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- META SECTION -->
<title>IPTV Admin Panel 1.5 - Welcome To The Dashboard</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<!-- END META SECTION -->

<!-- CSS INCLUDE -->
<link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
<!-- EOF CSS INCLUDE -->
</head>
<body class="page-container-boxed">
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container">
<?php $logout = new Select_DB(); ?>
<!-- START PAGE SIDEBAR -->
<div class="page-sidebar"> 
  <!-- START X-NAVIGATION -->
  <ul class="x-navigation">
    <li class="xn-logo"> <a href="admin.php"></a> <a href="#" class="x-navigation-control"></a> </li>
    <li class="xn-profile"> <a href="#" class="profile-mini"> <img src="assets/images/users/avatar.jpg" alt="IPTV-Admin 1.5"/> </a>
      <div class="profile">
        <div class="profile-image"> <img src="assets/images/users/avatar.jpg" alt="IPTV Admin 1.5"/> </div>
        <div class="profile-data">
          <div class="profile-data-name">Welcome</div>
          <div class="profile-data-title"><a href="<?php echo SITE_URL; ?>profile.php" style="color:#999;"><?php echo $_SESSION['user_info']['user_name']; ?></a></div>
        </div>
        <!--<div class="profile-controls"> <a href="#" class="profile-control-left"><span class="fa fa-info"></span></a> <a href="#" class="profile-control-right"><span class="fa fa-envelope"></span></a> </div>-->
      </div>
    </li>
    <li class="xn-title">Menu</li>
    <?php
	if($_SESSION['user_info']['user_type'] == 1 && $_SESSION['user_info']['user_id'] == 1 && !empty($_SESSION)){
	?>
    <li class="active"> <a href="admin.php"><img src="images/woo/home_32.png" style="margin-top: -5px;width:25px;"> <span class="xn-text">Dashboard</span></a> </li>
    <li id="channel_li" class="xn-openable"> <a href="#"><img src="images/woo/channel.png" style="margin-top: -5px;width:20px;"> <span class="xn-text"> Channels</span></a>
      <ul class="channels">
        <li><a href="channels.php"><span class="fa fa-image"></span> Channels Management</a></li>
        <li><a href="import_channel.php"><span class="fa fa-image"></span> Import Channels</a></li>
        <li><a href="add_channel.php"><span class="fa fa-image"></span> Add Channels</a></li>
      </ul>
    </li>
    <li id="categories_li" class="xn-openable"> <a href="#"><img src="images/woo/category.png" style="margin-top: -5px;width:20px;"><span class="xn-text"></span> Categories</a>
      <ul class="categories">
        <li><a href="category_management.php"><span class="fa fa-image"></span> Category Management</a></li>
        <li><a href="add_category.php"><span class="fa fa-image"></span> Add Category</a></li>
      </ul>
    </li>
    <li id="devices_li" class="xn-openable"> <a href="#"><img src="images/woo/device.png" style="margin-top: -5px;width:20px;"> <span class="xn-text"></span> Clients Management</a>
      <ul class="devices">
        <li><a href="device_management.php"><span class="fa fa-image"></span> Clients</a></li>
        <li><a href="add_device.php"><span class="fa fa-image"></span> Add New Client</a></li>
      </ul>
    </li>
    <li id="reseller_li" class="xn-openable"> <a href="#"><span class="fa fa-users"></span> Reseller Management</a>
      <ul class="reseller">
        <li><a href="reseller_management.php"><span class="fa fa-users"></span> Resellers</a></li>
        <li><a href="add_reseller.php"><span class="fa fa-user"></span> Add Reseller</a></li>
      </ul>
    </li>
    <li id="attached_li" class="xn-openable"> <a href="#"><img src="images/woo/device-attach.png" style="margin-top: -5px;width:20px;"> <span class="xn-text"> Activated Devices</span></a>
      <ul class="attached">
        <li><a href="attach_device.php"><span class="fa fa-image"></span> Device Activation</a></li>
      </ul>
    </li>
    <li id="content_li" class="xn-openable"> <a href="#"><img src="images/woo/info.png" style="margin-top: -5px;width:20px;"> <span class="xn-text"> Contents</span></a>
      <ul class="content">
        <li><a href="content_management.php"><span class="fa fa-image"></span> About Us Page</a></li>
      </ul>
    </li>
    <li id="streams_li" class="xn-openable"> <a href="#"><img src="images/woo/get-streams.png" style="margin-top: -5px;width:20px;"><span class="xn-text"></span> Streams</a>
      <ul class="streams">
        <li><a href="get_streams.php"><span class="fa fa-image"></span> Get Stream</a></li>
        <li><a href="#"><span class="fa fa-image"></span> Test Stream</a></li>
      </ul>
    </li>
    <?php
	}
	else { ?>
    <li class="active" id="dashboard"> <a href="admin.php"><img src="images/woo/home_32.png" style="margin-top: -5px;width:25px;"> <span class="xn-text">Dashboard</span></a> </li>
    <li id="devices_li" class="xn-openable"> <a href="#"><img src="images/woo/device.png" style="margin-top: -5px;width:20px;"> <span class="xn-text"></span> Clients Management</a>
      <ul class="devices">
        <li><a href="device_management.php"><span class="fa fa-image"></span> Clients</a></li>
        <li><a href="add_device.php"><span class="fa fa-image"></span> Add New Client</a></li>
      </ul>
    </li>
    <li id="attached_li" class="xn-openable"> <a href="#"><img src="images/woo/device-attach.png" style="margin-top: -5px;width:20px;"> <span class="xn-text"> Activated Devices</span></a>
      <ul class="attached">
        <li><a href="attach_device.php"><span class="fa fa-image"></span> Device Activation</a></li>
      </ul>
    </li>
    <?php
	} ?>
    <li id="logout"><a href="<?php echo SITE_URL; ?>logout.php"><span class="fa fa-sign-out"></span> Logout</a></li>
  </ul>
  <!-- END X-NAVIGATION --> 
</div>
<!-- END PAGE SIDEBAR -->