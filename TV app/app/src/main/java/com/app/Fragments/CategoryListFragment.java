package com.app.Fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.app.Adapters.ExpandableListAdapter;
import com.app.Loaders.CategoryLoader;
import com.app.Objects.Categories;
import com.app.Utils.ConnectivityTester;
import com.app.Utils.Utils;
import com.app.iptvadmin.DeviceIdPopupMain;
import com.app.iptvadmin.R;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

public class CategoryListFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<List<Categories>> {
    private static final int PERMISSION_PHONE_STATE = 1;
    private static final int PERMISSION_REQUEST_CODE = 2;
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    private Callbacks mCallbacks = sDummyCallbacks;
    private int mActivatedPosition = ListView.INVALID_POSITION;
    private final int _show_channels_on_item1 = 0;
    private List<Categories> _categories = null;

    private List<Categories> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<Categories, List<Categories>> _listDataChild;

    ExpandableListAdapter exAdapter;

    public interface Callbacks {

        public void onCategorySelected(int id, int subId);
    }

    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onCategorySelected(int id, int subId) {
        }
    };

    private ExpandableListView listView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.categories_list_layout, container, false);
        listView = (ExpandableListView) rootView.findViewById(R.id.expandable_listview);
        setListClickListener();
        return rootView;
    }

    private void setListClickListener() {
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Categories mainCategory = (Categories) exAdapter.getGroup(groupPosition);
                Categories subCategory = (Categories) exAdapter.getChild(groupPosition, childPosition);
                mCallbacks.onCategorySelected(mainCategory.getCategoryId(), subCategory
                        .getCategoryId());
                return false;
            }
        });

        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View view, int groupPosition, long id) {
                if (parent.isGroupExpanded(groupPosition)) {
                    Categories categories = (Categories) exAdapter.getGroup(groupPosition);
                    mCallbacks.onCategorySelected(categories
                            .getCategoryId(), 0);
                } else {

                }
                return false;
            }
        });

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setSelector(R.drawable.item_selector);

        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState
                    .getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConnectivityTester internet = new ConnectivityTester(getActivity());
        if (internet.isConnected()) {
            if (!checkPSPermission()) {
                requestPSPermission();
            } else {
                //Log.e("Device Id", Utils.getDeviceImei(this));
                //Toast.makeText(getActivity(), "Your device id is : " + Utils.getDeviceId(getActivity()), Toast.LENGTH_LONG).show();
                getLoaderManager().initLoader(0, null, this);
                getLoaderManager().getLoader(0).forceLoad();
                allPermission();
            }

        } else
            internet.detectInternetConnection();
    }

    private void allPermission() {
       /* if (checkStoragePermission()) {
            Toast.makeText(getActivity(), "Permission granted for storage.", Toast.LENGTH_LONG).show();
        } else {
            requestStoragePermission();
            Toast.makeText(getActivity(), "Please request permission.", Toast.LENGTH_LONG).show();
        }*/
        ArrayList<String> permissions = checkAllPermissions();
        if (permissions.size() == 0) {

        } else {
            String[] strArray = new String[permissions.size()];
            strArray = permissions.toArray(strArray);
            ActivityCompat.requestPermissions(getActivity(), strArray, PERMISSION_REQUEST_CODE);
        }
    }

    private ArrayList<String> checkAllPermissions() {
        ArrayList<String> strings = new ArrayList<>();
        int resultStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (resultStorage != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        int resultLocation = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (resultLocation != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        int resultCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        if (resultCamera != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.CAMERA);
        }
        int resultContacts = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.GET_ACCOUNTS);
        if (resultContacts != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.GET_ACCOUNTS);
        }
        int resultMicrophone = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO);
        if (resultMicrophone != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.RECORD_AUDIO);
        }
        return strings;
    }

   /* private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(getActivity(), "requestPermissions.", Toast.LENGTH_LONG).show();
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    private boolean checkStoragePermission() {
        int resultStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (resultStorage == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            //requestStoragePermission();
            return false;
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission Granted, Now you can access data.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Permission Denied, You cannot access data.", Toast.LENGTH_LONG).show();
                }
                break;
            case PERMISSION_PHONE_STATE:
                if (grantResults != null && grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLoaderManager().initLoader(0, null, this);
                    getLoaderManager().getLoader(0).forceLoad();
                    Toast.makeText(getActivity(), "Your device id is : " + Utils.getDeviceId(getActivity()), Toast.LENGTH_LONG).show();
                    allPermission();
                } else {
                    if (!checkPSPermission()) {
                        requestPSPermission();
                    } else {

                    }
                }

                break;
        }
    }

    private boolean checkPSPermission() {
        int resultStorage = ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.READ_PHONE_STATE);
        return resultStorage == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPSPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.READ_PHONE_STATE)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        PERMISSION_PHONE_STATE);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        PERMISSION_PHONE_STATE);
            }
        }
    }

    @Override
    public Loader<List<Categories>> onCreateLoader(int arg0, Bundle arg1) {
        return new CategoryLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<Categories>> arg0,
                               List<Categories> category) {
        if (category != null) {
            this._categories = category;
            this._listDataHeader = _categories;
            _listDataChild = new HashMap<>();
            for (Categories categories : _listDataHeader) {
                _listDataChild.put(categories, categories.getSubCategories());
            }
            exAdapter = new ExpandableListAdapter(getActivity(), _listDataHeader, _listDataChild);
            /*CategoryAdapter adapter = new CategoryAdapter(getActivity(),
                    R.layout.category_list_item, category);*/
            listView.setAdapter(exAdapter);
            setActivatedPosition(0);
            handler.sendEmptyMessage(_show_channels_on_item1);
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == _show_channels_on_item1) {
                if (!_categories.isEmpty()) {
                    mCallbacks.onCategorySelected(_categories.get(0)
                            .getCategoryId(), 0);
                    listView.requestFocus();
                } else {
                    Intent i = new Intent(getActivity(), DeviceIdPopupMain.class);
                    startActivity(i);
                    Toast.makeText(
                            getActivity(),
                            "Please Add categories and channels from Admin Panel",
                            Toast.LENGTH_LONG).show();
                }
            }
        }
    };

    @Override
    public void onLoaderReset(Loader<List<Categories>> arg0) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = sDummyCallbacks;
    }

	/*@Override
    public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);

		if (this._categories != null) {
			mCallbacks.onCategorySelected(this._categories.get(position)
					.getCategoryId());
			// setActivatedPosition(position);
		}
	}*/

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    public void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            listView.setItemChecked(mActivatedPosition, false);
        } else {
            listView.setItemChecked(position, true);
        }
        mActivatedPosition = position;
    }
}
