package com.app.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.app.Adapters.ChannelAdaptor;
import com.app.Loaders.ChannelLoader;
import com.app.Objects.Channel;
import com.app.Utils.ConnectivityTester;
import com.app.Utils.MyProgressDialog;
import com.app.iptvadmin.ChannelPlayerActivity;
import com.app.iptvadmin.R;

import java.util.ArrayList;
import java.util.List;

public class ChannelFragment extends Fragment implements
        TextWatcher, LoaderManager.LoaderCallbacks<List<Channel>> {
    private static final int PERMISSION_PHONE_STATE = 1;
    private static final int PERMISSION_REQUEST_CODE = 2;
    public static final String CATEGORY_ID = "category_id";
    public static final String SUB_CATEGORY_ID = "sub_category_id";
    public static String _categoryId = null;
    public static String _subcategoryId = null;
    private List<Channel> _channel = null;
    private GridView _channelGridView;
    private ChannelAdaptor _channelAdaptor;
    private static View _rootView;
    private MyProgressDialog progressDialog;
    private EditText editText;
    private ArrayList<Channel> channelList;
    private ArrayList<Channel> searhedChannels = new ArrayList<Channel>();
    private Context context;
    InputMethodManager imgr;

    @SuppressLint("ValidFragment")
    public ChannelFragment(Context context) {
        this.context = context;
    }

    public ChannelFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(CATEGORY_ID)) {
            ChannelFragment._categoryId = getArguments().getString(CATEGORY_ID);
            Log.e("category id", _categoryId);
        }

        ConnectivityTester internet = new ConnectivityTester(getActivity());
        if (internet.isConnected()) {
            if (!checkPSPermission()) {
                requestPSPermission();
            } else {
                getActivity().getSupportLoaderManager().initLoader(0, null, this);
                getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
                allPermission();
            }
        } else {
            internet.detectInternetConnection();
        }

    }

    private void allPermission() {
       /* if (checkStoragePermission()) {
            Toast.makeText(getActivity(), "Permission granted for storage.", Toast.LENGTH_LONG).show();
        } else {
            requestStoragePermission();
            Toast.makeText(getActivity(), "Please request permission.", Toast.LENGTH_LONG).show();
        }*/
        ArrayList<String> permissions = checkAllPermissions();
        if (permissions.size() == 0) {

        } else {
            String[] strArray = new String[permissions.size()];
            strArray = permissions.toArray(strArray);
            ActivityCompat.requestPermissions(getActivity(), strArray, PERMISSION_REQUEST_CODE);
        }
    }

    private ArrayList<String> checkAllPermissions() {
        ArrayList<String> strings = new ArrayList<>();
        int resultStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (resultStorage != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        int resultLocation = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (resultLocation != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        int resultCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        if (resultCamera != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.CAMERA);
        }
        int resultContacts = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.GET_ACCOUNTS);
        if (resultContacts != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.GET_ACCOUNTS);
        }
        int resultMicrophone = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO);
        if (resultMicrophone != PackageManager.PERMISSION_GRANTED) {
            strings.add(Manifest.permission.RECORD_AUDIO);
        }
        return strings;
    }

   /* private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(getActivity(), "requestPermissions.", Toast.LENGTH_LONG).show();
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    private boolean checkStoragePermission() {
        int resultStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (resultStorage == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            //requestStoragePermission();
            return false;
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission Granted, Now you can access data.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Permission Denied, You cannot access data.", Toast.LENGTH_LONG).show();
                }
                break;
            case PERMISSION_PHONE_STATE:
                if (grantResults != null && grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLoaderManager().initLoader(0, null, this);
                    getLoaderManager().getLoader(0).forceLoad();
                    allPermission();
                } else {
                    if (!checkPSPermission()) {
                        requestPSPermission();
                    } else {

                    }
                }

                break;
        }
    }

    private boolean checkPSPermission() {
        int resultStorage = ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.READ_PHONE_STATE);
        return resultStorage == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPSPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.READ_PHONE_STATE)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        PERMISSION_PHONE_STATE);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        PERMISSION_PHONE_STATE);
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        _rootView = inflater.inflate(R.layout.fragment_package_channels, container, false);
        if (_rootView != null) {
            editText = (EditText) _rootView.findViewById(R.id.search_editTxt);
            editText.addTextChangedListener(this);

			/*imgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);*/

            this.getActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            showProgressDialog();
        }
        return _rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (_rootView != null) {
            editText = (EditText) _rootView.findViewById(R.id.search_editTxt);
            editText.addTextChangedListener(this);
            /*if(imgr != null) {
                imgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
			}*/
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*if(imgr != null) {
            imgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
		}*/
    }

    @Override
    public Loader<List<Channel>> onCreateLoader(int arg0, Bundle arg1) {
        return new ChannelLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<Channel>> arg0, List<Channel> channel) {
        viewUpdateHandler.sendEmptyMessage(0);
        if (channel != null) {
            this._channel = channel;
            this.channelList = (ArrayList<Channel>) channel;
            this._channelAdaptor = new ChannelAdaptor(getActivity(), R.layout.childlistitem, (ArrayList<Channel>) channel);
            this._channelGridView = ((GridView) _rootView.findViewById(R.id.channel_gridView));
            this._channelGridView.setSelector(R.drawable.channel_item_selector);
            this._channelGridView.setOnItemClickListener(listener);
            _channelGridView.setAdapter(_channelAdaptor);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Channel>> arg0) {
    }

    private OnItemClickListener listener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                long id) {
            Intent intent = new Intent(getActivity(), ChannelPlayerActivity.class);
            intent.putExtra("stream_url", _channel.get(position).getStreamUrl());
            intent.putExtra("channel_name", _channel.get(position).getTitle());
            startActivity(intent);
        }
    };

    Handler viewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            super.handleMessage(msg);
        }

    };

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        /*if(editText.getText().toString().trim().length() > 0) {

			for(int i = 0 ; i< channelList.size() ; i++) {
				Channel chl = channelList.get(i);
				if(chl.getTitle().toLowerCase().contains(s.toString().toLowerCase())) {
					if(!searhedChannels.isEmpty()) {
						if(searhedChannels.indexOf(chl) == -1) {
							searhedChannels.add(chl);
						}
					}
					else {
						searhedChannels.add(chl);
					}
				}
			}
			if(!searhedChannels.isEmpty()) {
				_channelAdaptor.setChannel(searhedChannels);
				_channelAdaptor.notifyDataSetChanged();
			}
		}*/
        _channelAdaptor.getFilter().filter(s.toString());
    }

    private void showProgressDialog() {
        this.progressDialog = new MyProgressDialog(getActivity(), true);
        this.progressDialog.show();
    }

    private void dismissProgressDialog() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }

}
