package com.app.Objects;

import java.util.ArrayList;

public class Categories {
	
	private int categoryId;
	private String categoryName;
	private String categoryUrl;
	private ArrayList<Categories> subCategories;

	public ArrayList<Categories> getSubCategories() {
		return subCategories;
	}
	public void setSubCategories(ArrayList<Categories> subCategories) {
		this.subCategories = subCategories;
	}

	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryUrl() {
		return categoryUrl;
	}
	public void setCategoryUrl(String categoryUrl) {
		this.categoryUrl = categoryUrl;
	}
	
}
