package com.app.Adapters;

import java.util.ArrayList;
import java.util.List;
import com.app.Objects.Channel;
import com.app.Utils.ImageLoader;
import com.app.iptvadmin.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ChannelAdapter extends ArrayAdapter<Channel> {
	
	private Context CurrentContext;
	private Channel CurrentItem;
	private ArrayList<Channel> items;
	private final ImageLoader imageLoader;

	public ChannelAdapter(Context context, int textViewResourceId,
			List<Channel> objects) {
		super(context, textViewResourceId, objects);
		
		this.imageLoader = new ImageLoader(context,
				R.drawable.icon_video, R.drawable.icon_video);
		
		this.items = (ArrayList<Channel>) objects;
		this.CurrentContext = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if ((this.items == null) || (position + 1 > this.items.size()))
			return convertView;

		this.CurrentItem = ((Channel) this.items.get(position));
		LayoutInflater vi = (LayoutInflater) CurrentContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = vi.inflate(R.layout.channel_list_item, null);

		TextView channelName = (TextView) convertView
				.findViewById(R.id.channel_name_txt);
		ImageView channelLogo = (ImageView)convertView
				.findViewById(R.id.list_image);
				
		channelName.setText(CurrentItem.getTitle());
		if(CurrentItem.getImage() != null){
			channelLogo.setTag(imageLoader);
			imageLoader.displayImage(CurrentItem.getImage(), channelLogo);
		}else{
			channelLogo.setImageResource(R.drawable.icon_video);
		}
			
		return convertView;
	}

}
