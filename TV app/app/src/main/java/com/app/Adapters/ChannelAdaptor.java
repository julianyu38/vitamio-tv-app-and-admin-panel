package com.app.Adapters;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;

import com.app.Objects.Channel;
import com.app.Utils.ImageLoader;
import com.app.iptvadmin.R;

public class ChannelAdaptor extends ArrayAdapter<Channel> {

    private ArrayList<Channel> items;
    private Context CurrentContext;
    private Channel CurrentItem;
    private final ImageLoader imageLoader;
    private ChannelFilter channelFilter;

    public ChannelAdaptor(Context context, int textViewResourceId,
                          ArrayList<Channel> items) {
        super(context, textViewResourceId, items);

        this.imageLoader = new ImageLoader(context,
                R.drawable.thumb_loading_small, R.drawable.thumb_loading_small);
        this.items = items;
        CurrentContext = context;
    }

    public void setChannel(ArrayList<Channel> channels) {
        this.items = channels;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        VideoItemViewHolder itemHolder = null;

        // verify that the items list is still valid since
        // the list may have been cleared during an update
        if ((items == null) || ((position + 1) > items.size()))
            return convertView; // Can't extract item

        CurrentItem = (Channel) items.get(position);

        if (convertView == null) {

            LayoutInflater vi = (LayoutInflater) CurrentContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.childlistitem, null);
            itemHolder = new VideoItemViewHolder();

            itemHolder.ivThumb = (ImageView) convertView
                    .findViewById(R.id.channel_image);
            /*itemHolder.tvName = (TextView) convertView
                    .findViewById(R.id.chanel_name);*/
            itemHolder.ivThumb.setAdjustViewBounds(true);

            convertView.setTag(itemHolder);
        } else {

            itemHolder = (VideoItemViewHolder) convertView.getTag();

        }

        if (CurrentItem.getImage() != null && CurrentItem.getTitle() != null) {
            itemHolder.ivThumb.setTag(imageLoader);
            imageLoader
                    .displayImage(CurrentItem.getImage(), itemHolder.ivThumb);
            //itemHolder.tvName.setText(CurrentItem.getTitle());

        } else {
            itemHolder.ivThumb.setImageResource(R.drawable.channel);
        }

        // itemHolder.tvTitle.setText(CurrentItem.getTitle());
        // itemHolder.tvComments.setText(CurrentItem.getDescription());

        return convertView;
    }

    private class VideoItemViewHolder {

        ImageView ivThumb;
        //TextView tvName;
    }

    @Override
    protected void finalize() {
        try {
            super.finalize();
            imageLoader.clearCache();
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    @Override
    public Filter getFilter() {
        if (channelFilter == null)
            channelFilter = new ChannelFilter();

        return channelFilter;
    }

    @SuppressLint("DefaultLocale")
    private class ChannelFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = items;
                results.count = items.size();
            } else {
                // We perform filtering operation
                List<Channel> nChannelList = new ArrayList<Channel>();

                for (Channel channel : items) {
                    if (channel.getTitle().toUpperCase()
                            .contains(constraint.toString().toUpperCase()))
                        nChannelList.add(channel);
                }

                results.values = nChannelList;
                results.count = nChannelList.size();

            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            items = (ArrayList<Channel>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0; i < items.size(); i++) {
                add(items.get(i));
                notifyDataSetInvalidated();
            }
            /*if (results.count == 0)
                notifyDataSetInvalidated();
			else {
				items = (ArrayList<Channel>) results.values;
				notifyDataSetChanged();
				clear();
			}*/

        }

    }

}
