package com.app.iptvadmin;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.app.Utils.Utils;

public class DeviceIdPopupMain extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.id_popup_main);
        TextView tv_id = (TextView) findViewById(R.id.tvDeviceId);
        tv_id.setText(Utils.getDeviceImei(this));
    }
}
