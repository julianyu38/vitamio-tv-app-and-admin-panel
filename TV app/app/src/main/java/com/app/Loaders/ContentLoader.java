package com.app.Loaders;

import org.json.JSONArray;
import org.json.JSONObject;
import com.app.Utils.JSONParser;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

public class ContentLoader extends AsyncTaskLoader<String> {

	public ContentLoader(Context context) {
		super(context);
	}

	@Override
	public String loadInBackground() {

		String url = "http://0331.iptv.rocks/service/about_us.php";
		String content = "";
		try {

			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			JSONObject json = jParser.getJSONFromUrl(url);
			JSONArray jsonArray = json.getJSONArray("about_us");
			
			JSONObject result = jsonArray.getJSONObject(0);
			
			content = result.getString("content");
			Log.e("About us", content);
			//s1 = content.split(">");
			//s2 = s1[1].split("<");

		} catch (Exception e) {

		}
		return content;
	}

}
