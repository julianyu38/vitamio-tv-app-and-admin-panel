package com.app.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.KeyEvent;

public class MyProgressDialog extends ProgressDialog {
    Context _context;
    Activity _activity;
    boolean _closeActivityOnBackKey;

    public MyProgressDialog(Context context, boolean closeActivityOnBackKey) {
        super(context);
        this._context = context;
        this._activity = ((Activity) this._context);
        this._closeActivityOnBackKey = closeActivityOnBackKey;

        this.setTitle("");
        this.setMessage("Loading...");
        this.setIndeterminate(true);
        this.setCancelable(false);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // handle the keypress
            this.dismiss();

            if (this._closeActivityOnBackKey) {
                this._activity.finish();
            }

            return true;
        } else {
            return false;
        }
    }
}