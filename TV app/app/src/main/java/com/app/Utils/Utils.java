package com.app.Utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

public class Utils {

	// TODO: group with inner classes

	public static final String TAG = "Utils";

	public static final String CACHE_DIR_NAME = "__vimeo_v_cache";

	public enum VideoQuality {
		MOBILE, SD, HD
	};

	private static File cacheDir = null;
	private static boolean cacheDirCreated = false;

	// ------------------------ Files: Streams / Cache -------------------------

	public static void copyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static void copyFile(File oldLocation, File newLocation)
			throws IOException {
		if (oldLocation.exists()) {
			BufferedInputStream reader = new BufferedInputStream(
					new FileInputStream(oldLocation));
			BufferedOutputStream writer = new BufferedOutputStream(
					new FileOutputStream(newLocation, false));
			try {
				byte[] buff = new byte[8192];
				int numChars;
				while ((numChars = reader.read(buff, 0, buff.length)) != -1) {
					writer.write(buff, 0, numChars);
				}
			} catch (IOException ex) {
				throw new IOException("IOException when transferring "
						+ oldLocation.getPath() + " to "
						+ newLocation.getPath());
			} finally {
				try {
					if (reader != null) {
						writer.close();
						reader.close();
					}
				} catch (IOException ex) {
					Log.e(TAG,
							"Error closing files when transferring "
									+ oldLocation.getPath() + " to "
									+ newLocation.getPath());
				}
			}
		} else {
			throw new IOException(
					"Old location does not exist when transferring "
							+ oldLocation.getPath() + " to "
							+ newLocation.getPath());
		}
	}

	public static File createCacheDir(Context context, String dirName) {
		File preparedDir;
		if (android.os.Environment.MEDIA_MOUNTED.equals(android.os.Environment
				.getExternalStorageState())) {
			preparedDir = context.getDir(dirName /*
												 * +
												 * UUID.randomUUID().toString()
												 */, Context.MODE_PRIVATE);
			Log.i(TAG,
					"Cache dir initialized at SD card "
							+ preparedDir.getAbsolutePath());
		} else {
			preparedDir = context.getCacheDir();
			Log.i(TAG,
					"Cache dir initialized at phone storage "
							+ preparedDir.getAbsolutePath());
		}
		if (!preparedDir.exists()) {
			Log.i(TAG, "Cache dir not existed, creating");
			preparedDir.mkdirs();
		}
		return preparedDir;
	}

	public static File getDefaultCacheDir(Context context) {
		if (cacheDirCreated)
			return cacheDir;
		else {
			cacheDir = createCacheDir(context, CACHE_DIR_NAME);
			cacheDirCreated = true;
			return cacheDir;
		}
	}

	public static File newTempFile(Context context, String prefix, String suffix)
			throws IOException {
		return File.createTempFile(prefix, suffix, getDefaultCacheDir(context));
	}

	public static long computeFreeSpace() {
		File dataDir = Environment.getDataDirectory();
		StatFs stat = new StatFs(dataDir.getPath());
		return stat.getAvailableBlocks() * stat.getBlockSize();
	}

	// ------------------------ Views ------------------------------------------

	public static View getItemViewIfVisible(AdapterView<?> holder, int itemPos) {
		int firstPosition = holder.getFirstVisiblePosition();
		int wantedChild = itemPos - firstPosition;
		if (wantedChild < 0 || wantedChild >= holder.getChildCount())
			return null;
		return holder.getChildAt(wantedChild);
	}

	public static void invalidateByPos(AdapterView<?> parent, int position) {
		final View itemView = getItemViewIfVisible(parent, position);
		Log.d(TAG, "Trying to invalidate view " + itemView + " at pos "
				+ position + " ");
		if (itemView != null)
			itemView.invalidate();
	}

	public static void forcePostInvalidate(AdapterView<?> parent, int position) {
		final View itemView = getItemViewIfVisible(parent, position);
		if (itemView != null)
			itemView.postInvalidate();
	}

	public static String getDeviceId(Context context) {
		FragmentActivity activity = ((android.support.v4.app.FragmentActivity) context);

		final TelephonyManager tm = (TelephonyManager) activity
				.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
		String tmDevice = "" + tm.getDeviceId();
		String tmSerial = "" + tm.getSimSerialNumber();

		String androidId = ""
				+ android.provider.Settings.Secure.getString(
						activity.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

		String deviceId = deviceUuid.toString().replaceAll("-", "");

		return deviceId;
	}

	
	public static String getDeviceImei(Context context) {

		FragmentActivity activity = ((android.support.v4.app.FragmentActivity) context);

		// check deviceId in shared preferences and get from it
		SharedPreferences preferences;
		SharedPreferences.Editor editor;

		preferences = context.getSharedPreferences("device_info",
				Context.MODE_PRIVATE);
		editor = preferences.edit();

		String deviceId = preferences.getString("deviceId", null);

		// if deviceId in not stored in shared preferences then generate it &
		// store it in preferences
		if (deviceId == null) {
			final TelephonyManager tm = (TelephonyManager) activity
					.getBaseContext().getSystemService(
							Context.TELEPHONY_SERVICE);
			String tmDevice = "" + tm.getDeviceId();
			String tmSerial = "" + tm.getSimSerialNumber();

			String androidId = ""
					+ android.provider.Settings.Secure.getString(
							activity.getContentResolver(),
							android.provider.Settings.Secure.ANDROID_ID);

			UUID deviceUuid = new UUID(androidId.hashCode(),
					((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

			deviceId = deviceUuid.toString().replaceAll("-", "");

			// save deviceId in shared preferences to be retrieved from it later
			editor.putString("deviceId", deviceId);
			editor.commit();
		}

		// clear shared preferences objects
		context = null;
		editor = null;
		deviceId = deviceId.substring (5);

		return deviceId;
	}

}
