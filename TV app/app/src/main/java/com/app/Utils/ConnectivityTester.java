package com.app.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;


public class ConnectivityTester {
	
  private Activity activity;
  private ConnectivityManager connectivityManager;
  
  
  public ConnectivityTester(Activity activity){
    this.activity = activity;
    setup(); 
  }
  
  private void setup() {
    
    connectivityManager = ( ConnectivityManager ) activity.getSystemService( Context.CONNECTIVITY_SERVICE );
   
  }
  
  //returns true if currently connected and false otherwise
  public boolean isConnected() {
    try {
      return connectivityManager.getActiveNetworkInfo().isConnected();
    }
    catch (NullPointerException npe) {
      return false;
    }
  }
 
    //determines if currently connected to the Internet
    //if user is not connected, he/she is asked to connect to the Internet via an alert
    //dialog box and the app closes
    public void detectInternetConnection () {
       if (!isConnected()) {
           System.out.println("NOT CONNECTED");
           AlertDialog.Builder builder = new AlertDialog.Builder(activity);
           builder
             .setMessage("No Internet Connection Detected! Please fix and try again.")
             .setCancelable(false)
               .setPositiveButton("OK", 
                   new DialogInterface.OnClickListener() {
                          public void onClick(DialogInterface dialog, int id) {
                            activity.finish();
                          }
                      }
               )
               .setTitle("Internet Connection Problem")
               .show();
           
         }
         else {
           System.out.println("Internet Connection Detected");
         }
    }
  
}
